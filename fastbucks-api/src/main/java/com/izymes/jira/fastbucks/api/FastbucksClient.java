package com.izymes.jira.fastbucks.api;


import java.util.List;
import java.util.Map;
import java.util.Set;

public interface FastbucksClient {
    public void setConfig(Map<String, String> configMap);
    public Map<String, String> getConfig( );
    public List<String> getConfigKeys();
    public void setConfigKeys( List<String> configKeys );
    public GenericResponse testConnection( TEST_ENDPOINT endpoint, Object... args );
    public GenericResponse sendInvoice( GenericInvoice genericInvoice );
    public String getName();

    public enum TEST_ENDPOINT{
        connection,
        contact
    };
}
