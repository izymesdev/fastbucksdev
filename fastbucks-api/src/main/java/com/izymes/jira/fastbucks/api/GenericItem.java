package com.izymes.jira.fastbucks.api;

import java.util.Date;

public class GenericItem {
    private Date date;
    private String name;
    private String description;
    private float quantity;
    private float unitCost;

    GenericItem(Date date, String name, String description, float quantity, float unitCost) {
        this.date = date;
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.unitCost = unitCost;
    }

    public Date getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public float getQuantity() {
        return quantity;
    }

    public float getUnitCost() {
        return unitCost;
    }
}
