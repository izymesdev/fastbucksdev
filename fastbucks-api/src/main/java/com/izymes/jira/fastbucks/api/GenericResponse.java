package com.izymes.jira.fastbucks.api;

public class GenericResponse {
    private final StringBuilder message = new StringBuilder();
    private GenericInvoice confirmedInvoice;
    private Exception exception;


    public void addMessage( String msg) {
        message.append( msg );
    }

    public String getMessage() {
        return message.toString();
    }

    public GenericInvoice getConfirmedInvoice() {
        return confirmedInvoice;
    }

    public void setConfirmedInvoice(GenericInvoice confirmedInvoice) {
        this.confirmedInvoice = confirmedInvoice;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
