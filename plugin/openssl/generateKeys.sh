#!/bin/sh
set date = `date`
tar -cvf backup.$date.tar mykey.* keys*
openssl genrsa -out mykey.pem 1024
openssl req -newkey rsa:1024 -x509 -key mykey.pem -out mykey.cer -days 365
#openssl pkcs12 -export -out keys.pfx -inkey mykey.pem -in mykey.cer
openssl pkcs8 -topk8 -inform PEM -outform DER -in mykey.pem -out mykey.der -nocrypt
