package com.izymes.jira.fastbucks.action;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.izymes.jira.fastbucks.modules.ClientManager;
import webwork.action.ServletActionContext;

import java.io.IOException;


public class FastbucksClientAction extends JiraWebActionSupport {

    private ClientManager clientManager;

    @Override
    protected String doExecute() throws Exception {

        String referer = ServletActionContext.getRequest().getHeader("referer");

        log.debug("Redirecting to: " + referer);
        ServletActionContext.getResponse().sendRedirect(referer);
        return NONE;

     }

    public String doDetect() throws IOException {

        String referer = ServletActionContext.getRequest().getHeader("referer");
        ServletActionContext.getResponse().sendRedirect(referer);

        clientManager.detectClients();

        return NONE;
    }

    public void setClientManager(ClientManager clientManager) {
        this.clientManager = clientManager;
    }
}
