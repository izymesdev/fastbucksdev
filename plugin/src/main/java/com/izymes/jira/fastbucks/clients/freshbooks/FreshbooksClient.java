package com.izymes.jira.fastbucks.clients.freshbooks;

import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.izymes.jira.fastbucks.api.FastbucksClient;
import com.izymes.jira.fastbucks.api.GenericInvoice;
import com.izymes.jira.fastbucks.api.GenericItem;
import com.izymes.jira.fastbucks.api.GenericResponse;
import com.izymes.jira.fastbucks.clients.freshbooks.exceptions.ConnectionException;
import com.izymes.jira.fastbucks.clients.freshbooks.model.*;
import com.izymes.jira.fastbucks.clients.freshbooks.model.System;
import com.izymes.jira.fastbucks.clients.freshbooks.service.*;
import com.opensymphony.util.TextUtils;
import org.apache.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class FreshbooksClient implements FastbucksClient {
    private static final Logger log = Logger.getLogger(FreshbooksClient.class);
    private List<String> configKeys;
    private Map<String, String> configMap;
    public final String name="Freshbooks";
    public static final long THIRTY_DAYS_MS = 30L * 24L * 60L * 60L * 1000L;
    public static final String URL_KEY = "com.izymes.jira.fastbucks.client.freshbooks.url";
    public static final String AUTH_TOKEN_KEY = "com.izymes.jira.fastbucks.client.freshbooks.authtoken";

    private final I18nResolver i18nResolver;
    private final FreshbooksServiceFactory freshbooksServiceFactory;
    private final SimpleDateFormat dateFormatter;
    public static long YEAR_OF_JIRA;

    public FreshbooksClient(I18nResolver i18nResolver, FreshbooksServiceFactory factory) {
        this.i18nResolver = i18nResolver;
        this.freshbooksServiceFactory = factory;
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
        try {
            YEAR_OF_JIRA = dateFormatter.parse("2002-01-01-00-00").getTime();
        } catch (ParseException e) {
            log.error("",e);
        }
    }

    @Override
    public void setConfig(Map<String, String> configMap) {
        if ( this.configMap == null ){
            this.configMap = configMap;
            return;
        }
        String currentToken = this.configMap.get(AUTH_TOKEN_KEY);
        String newToken = configMap.get( AUTH_TOKEN_KEY );
        if ( !TextUtils.stringSet( currentToken ) || TextUtils.stringSet( newToken )){
            this.configMap = configMap;
        } else if ( !TextUtils.stringSet( newToken ) ){
            this.configMap.put( URL_KEY, configMap.get(URL_KEY) );
            configMap.put(AUTH_TOKEN_KEY, currentToken );
        }
    }

    @Override
    public Map<String, String> getConfig() {
        return configMap;
    }

    @Override
    public List<String> getConfigKeys() {
        return configKeys;
    }

    @Override
    public void setConfigKeys(List<String> configKeys) {
        this.configKeys = configKeys;
    }

    @Override
    public GenericResponse testConnection(TEST_ENDPOINT endpoint, Object... args) {
        FreshbooksConnection connection = getConnection();
        GenericResponse testResponse = new GenericResponse();
        try {
            if (endpoint == TEST_ENDPOINT.contact) {
                final String contactName = (String) args[0];
                FreshbooksService<Clients> service = freshbooksServiceFactory.getService(ClientsService.class).setConnection(connection);
                Clients clients = service.get();
                Client theClient = Iterables.find( clients.getClients(), new Predicate<Client>() {
                    @Override
                    public boolean apply( Client input ) {
                        return contactName.toLowerCase().contains(input.getLastName().toLowerCase());
                    }
                });
                if ( theClient != null ){
                    testResponse.addMessage(i18nResolver.getText("fastbucks.project.config.client.test.contact", 1, contactName, theClient.getEmail(), "OK" ));
                } else {
                    throw new ConnectionException(String.format("Contact for % not found", contactName));
                }
            } else if (endpoint == TEST_ENDPOINT.connection) {
                FreshbooksService<System> service = freshbooksServiceFactory.getService(SystemService.class).setConnection(connection);
                System system = service.get();
                testResponse.addMessage(i18nResolver.getText("fastbucks.project.config.client.test.organisation", system.getCompanyName()));
            }
        } catch (ConnectionException e) {
            testResponse.setException( new Exception( e.getMessage() ));
            log.error("", e);
            return testResponse;
        }

        return testResponse;

    }

    private FreshbooksConnection getConnection() {
        String url = configMap.get(URL_KEY);
        String token = configMap.get(AUTH_TOKEN_KEY);
        return new FreshbooksConnectionImpl(url, token, "agent");
    }

    @Override
    public GenericResponse sendInvoice(GenericInvoice genericInvoice) {
        FreshbooksConnection connection = getConnection();
        ClientService clientService = freshbooksServiceFactory.getService(ClientService.class);
        clientService.setConnection(connection).setLastName( genericInvoice.getContactName() );
        FreshbooksService<Invoice> invoiceService = freshbooksServiceFactory.getService(InvoiceService.class).setConnection(connection);
        GenericResponse genericResponse = new GenericResponse();
        try {
            Client client = clientService.get();
            Invoice invoice = new Invoice();
            List<InvoiceLine> lines = Lists.transform( genericInvoice.getItems(), new Function<GenericItem, InvoiceLine>() {
                @Override
                public InvoiceLine apply( GenericItem item) {
                    InvoiceLine line = new InvoiceLine();
                    line.setName(item.getName());
                    line.setQuantity(item.getQuantity());
                    line.setDescription(item.getDescription());
                    line.setUnitCost(item.getUnitCost());
                    return line;
                }
            });

            invoice.setLines( Lists.newArrayList( lines ) );
            invoice.setClientId( client.getId() );
            invoice.setNumber( makeFreshbooksInvoiceNumber(genericInvoice.getInvoiceNumber()) );
            invoice.setStatus("draft");
            Response fbResponse = invoiceService.create( invoice );
            log.info("Freshbooks invoice " + genericInvoice.getInvoiceNumber() + " sent \n" + genericInvoice.toString());
            genericResponse.addMessage(i18nResolver.getText("fastbucks.invoice.sent", fbResponse.getInvoiceId()));
            genericInvoice.setInvoiceNumber( fbResponse.getInvoiceId()+"");
            genericResponse.setConfirmedInvoice( genericInvoice );
        } catch (ConnectionException e) {
            genericResponse.setException(new Exception(e.getMessage()));
        }
        return genericResponse;
    }

    private String makeFreshbooksInvoiceNumber(String invoiceNumber) throws ConnectionException {
        String[] invElements = invoiceNumber.split("-"); //INV-KEY-yyyy-MM-dd-HH-mm
        int keyLength = Math.min(4, invElements[1].length());
        String key = invElements[1].substring(0,keyLength);
        try {
            Date time = dateFormatter.parse(String.format("%s-%s-%s-%s-%s", invElements[2], invElements[3], invElements[4], invElements[5], invElements[6]));
            long timeSinceJira = time.getTime() - YEAR_OF_JIRA;

            String invNumber = key + Long.toHexString(timeSinceJira / 60000L);
            if (invNumber.length() > 10) throw new ConnectionException("freshbooks invoice number too long " + invNumber );
            return invNumber;
        } catch (ParseException e) {
            throw new ConnectionException( e.getMessage() );
        }
    }

    @Override
    public String getName() {
        return name;
    }
}
