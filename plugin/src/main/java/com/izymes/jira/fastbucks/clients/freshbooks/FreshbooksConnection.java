package com.izymes.jira.fastbucks.clients.freshbooks;

import com.izymes.jira.fastbucks.clients.freshbooks.exceptions.ConnectionException;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Request;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Response;

public interface FreshbooksConnection {

    /**
        * Send a request to the FreshBooks API and return the response object.
        * @param request
        * @return
        * @throws Error
        */

    public Response performRequest(Request request) throws ConnectionException;
}
