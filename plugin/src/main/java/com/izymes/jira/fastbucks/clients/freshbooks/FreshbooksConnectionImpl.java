package com.izymes.jira.fastbucks.clients.freshbooks;

import com.izymes.jira.fastbucks.clients.freshbooks.exceptions.ConnectionException;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Request;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Response;
import com.thoughtworks.xstream.XStream;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

public class FreshbooksConnectionImpl implements FreshbooksConnection {

    private final String url;
    private final String authToken;
    private final String userAgent;
    private HttpClient client;

    public FreshbooksConnectionImpl(String url, String authToken, String userAgent) {
        this.authToken = authToken;
        this.url = url;
        this.userAgent = userAgent;
    }


    @Override
    public Response performRequest(Request request) throws ConnectionException {
        XStream xs = new CustomXStream();

        String paramString = xs.toXML(request);
        PostMethod method = new PostMethod(url);
        method.setContentChunked(false);
        method.setDoAuthentication(true);
        method.setFollowRedirects(false);
        method.addRequestHeader("User-Agent", userAgent);
        //method.addRequestHeader("Authorization", base64key);
        try {
            method.setRequestEntity(new StringRequestEntity(paramString, "text/xml", "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        method.getParams().setContentCharset("utf-8");

        try {
            getClient().executeMethod(method);
            InputStream is = method.getResponseBodyAsStream();
/*
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, "UTF-8");
            System.out.println(writer.toString());
*/

            Response response = (Response) xs.fromXML(is);
            // TODO Throw an error if we got one
            if (response.isFail()) {
                throw new ConnectionException(response.getError());
            }
            return response;

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            throw new ConnectionException(e.getMessage());
        } finally {
            method.releaseConnection();
        }
    }

    private URL getUrl( String url ){
        try {
            return new URL( url );
        } catch (MalformedURLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    private HttpClient getClient() {
        if (client == null) {
            client = new HttpClient();
            client.getParams().setAuthenticationPreemptive(true);
            client.getState().setCredentials(new AuthScope(getUrl(url).getHost(), 443, AuthScope.ANY_REALM), new UsernamePasswordCredentials(authToken, ""));
        }
        return client;
    }

}
