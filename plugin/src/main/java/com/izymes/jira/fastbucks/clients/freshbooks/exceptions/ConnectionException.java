package com.izymes.jira.fastbucks.clients.freshbooks.exceptions;

public class ConnectionException extends Exception {
    public ConnectionException( String msg ){
        super( msg );
    }
}
