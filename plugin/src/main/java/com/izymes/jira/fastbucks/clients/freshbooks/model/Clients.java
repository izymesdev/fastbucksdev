package com.izymes.jira.fastbucks.clients.freshbooks.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.ArrayList;

@XStreamAlias("clients")
public class Clients extends PagedResponseContent<Client> {
    
    public ArrayList<Client> getClients() {
        return getContents();
    }
}
