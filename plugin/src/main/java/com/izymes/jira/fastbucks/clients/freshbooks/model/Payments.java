package com.izymes.jira.fastbucks.clients.freshbooks.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.ArrayList;

@XStreamAlias("payments")
public class Payments extends PagedResponseContent<Payment> {
    ArrayList<Payment> getPayments() {
        return getContents();
    }
}
