package com.izymes.jira.fastbucks.clients.freshbooks.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.io.Serializable;

@XStreamAlias("system")
public class System implements Serializable {

    @XStreamAlias("company_name")
    String companyName;
    String profession;
    @XStreamAlias("currency_code")
    String currencyCode;
    @XStreamAlias("api")
    Api api;

    public System(){}

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Api getApi() {
        return api;
    }

    public void setApi(Api api) {
        this.api = api;
    }

    @XStreamAlias("api")
    public static class Api implements Serializable{
        int requests;
        @XStreamAlias("request_limit")
        int requestLimit;

        public int getRequests() {
            return requests;
        }

        public void setRequests(int requests) {
            this.requests = requests;
        }

        public int getRequestLimit() {
            return requestLimit;
        }

        public void setRequestLimit(int requestLimit) {
            this.requestLimit = requestLimit;
        }
    }
}
