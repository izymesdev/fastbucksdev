package com.izymes.jira.fastbucks.clients.freshbooks.service;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.izymes.jira.fastbucks.clients.freshbooks.FreshbooksConnection;
import com.izymes.jira.fastbucks.clients.freshbooks.exceptions.ConnectionException;
import com.izymes.jira.fastbucks.clients.freshbooks.model.*;
import com.opensymphony.util.TextUtils;

public class ClientService implements FreshbooksService<Client> {

    protected FreshbooksConnection connection;
    protected Request request;
    protected String lastName;
    protected String email;
    protected String id;


    public ClientService() {
        this.request = new Request( RequestMethod.CLIENT_GET );
    }

    public ClientService setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public ClientService setEmail(String email) {
        this.email = email;
        return this;
    }

    public ClientService setId(String id) {
        this.id = id;
        return this;
    }

    @Override
    public Response create(Client arg) throws ConnectionException{
        throw new ConnectionException("Service not available");
    }

    @Override
    public ClientService setConnection(FreshbooksConnection connection) {
        this.connection = connection;
        return this;
    }

    public Client get() throws ConnectionException {
        if (TextUtils.stringSet( id )){
            request.setClientId( Long.parseLong(id) );
        } else if ( TextUtils.stringSet( email ) ){
            request.setEmail( email );
        } else if ( TextUtils.stringSet(lastName) ){
            Client client = getClientByLastName( lastName );
            return client;
        } else {
            throw new ConnectionException("Can't find client without name, email or id");
        }

        Response response = connection.performRequest( request );
        return response.getClient();
    }

    private Client getClientByLastName(final String lastName) throws ConnectionException {
        FreshbooksService<Clients> service = new ClientsService().setConnection(connection);
        Clients clients = service.get();
        Client theClient = Iterables.find(clients.getClients(), new Predicate<Client>() {
            @Override
            public boolean apply(Client input) {
                return lastName.toLowerCase().contains(input.getLastName().toLowerCase());
            }
        });
        return theClient;
    }

}
