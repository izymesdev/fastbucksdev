package com.izymes.jira.fastbucks.clients.freshbooks.service;

import com.izymes.jira.fastbucks.clients.freshbooks.FreshbooksConnection;
import com.izymes.jira.fastbucks.clients.freshbooks.exceptions.ConnectionException;
import com.izymes.jira.fastbucks.clients.freshbooks.model.*;

import java.util.ArrayList;

public class ClientsService implements FreshbooksService<Clients> {

    protected FreshbooksConnection connection;
    protected Request request;


    public ClientsService() {
        this.request = new Request( RequestMethod.CLIENT_LIST );
    }

    @Override
    public Response create(Clients arg) throws ConnectionException {
        throw new ConnectionException("service not available");
    }

    @Override
    public FreshbooksService<Clients> setConnection(FreshbooksConnection connection) {
        this.connection = connection;
        return this;
    }

    public Clients get() throws ConnectionException {
        Clients clients = new Clients();
        clients.setContents(getPage(1));
        return clients;
    }

    private ArrayList<Client> getPage( int page ) throws ConnectionException {
        request.setPage( page );
        Response fResponse = connection.performRequest( request );
        ArrayList<Client> clients = fResponse.getClients().getClients();
        if ( fResponse.getClients().getPages() > page ){
            clients.addAll( getPage(page + 1) );
            return clients;
        }
        return clients;

    }
}
