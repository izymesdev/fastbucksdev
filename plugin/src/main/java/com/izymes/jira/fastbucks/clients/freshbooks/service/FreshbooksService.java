package com.izymes.jira.fastbucks.clients.freshbooks.service;

import com.izymes.jira.fastbucks.clients.freshbooks.FreshbooksConnection;
import com.izymes.jira.fastbucks.clients.freshbooks.exceptions.ConnectionException;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Response;

public interface FreshbooksService<T> {
    public T get() throws ConnectionException;
    public Response create(T arg) throws ConnectionException;
    public FreshbooksService<T> setConnection(FreshbooksConnection connection);
}
