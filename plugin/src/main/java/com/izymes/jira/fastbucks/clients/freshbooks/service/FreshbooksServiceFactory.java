package com.izymes.jira.fastbucks.clients.freshbooks.service;


public interface FreshbooksServiceFactory {
    public <T> T getService(Class<T> clazz);
}
