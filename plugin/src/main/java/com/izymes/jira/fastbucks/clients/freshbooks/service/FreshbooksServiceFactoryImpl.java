package com.izymes.jira.fastbucks.clients.freshbooks.service;

public class FreshbooksServiceFactoryImpl implements FreshbooksServiceFactory {

    public <T> T getService(Class<T> clazz){
        try {
            return clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }
}
