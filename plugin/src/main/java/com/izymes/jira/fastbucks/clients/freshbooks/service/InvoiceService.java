package com.izymes.jira.fastbucks.clients.freshbooks.service;

import com.izymes.jira.fastbucks.clients.freshbooks.FreshbooksConnection;
import com.izymes.jira.fastbucks.clients.freshbooks.exceptions.ConnectionException;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Invoice;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Request;
import com.izymes.jira.fastbucks.clients.freshbooks.model.RequestMethod;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Response;

public class InvoiceService implements FreshbooksService<Invoice> {
    protected FreshbooksConnection connection;
    protected Request request;

    public InvoiceService() {
        this.request = new Request( RequestMethod.INVOICE_CREATE );
    }

    @Override
    public Invoice get() throws ConnectionException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public FreshbooksService<Invoice> setConnection(FreshbooksConnection connection) {
        this.connection = connection;
        return this;
    }

    @Override
    public Response create(Invoice arg) throws ConnectionException {
        request.setInvoice( arg );
        Response response = connection.performRequest( request );
        return response;
    }
}
