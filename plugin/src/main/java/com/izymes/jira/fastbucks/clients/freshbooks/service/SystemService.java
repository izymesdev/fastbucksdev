package com.izymes.jira.fastbucks.clients.freshbooks.service;

import com.izymes.jira.fastbucks.clients.freshbooks.FreshbooksConnection;
import com.izymes.jira.fastbucks.clients.freshbooks.exceptions.ConnectionException;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Request;
import com.izymes.jira.fastbucks.clients.freshbooks.model.RequestMethod;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Response;
import com.izymes.jira.fastbucks.clients.freshbooks.model.System;

public class SystemService implements FreshbooksService<System> {

    protected FreshbooksConnection connection;
    protected Request request;

    public SystemService() {
        this.request = new Request( RequestMethod.SYSTEM_CURRENT );
    }

    @Override
    public FreshbooksService<System> setConnection(FreshbooksConnection connection) {
        this.connection = connection;
        return this;
    }

    public System get() {
                try {
                    Response fResponse =  connection.performRequest( request );
                    return fResponse.getSystem();
                } catch (ConnectionException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
        return null;
    }

    @Override
    public Response create(System arg) throws ConnectionException {
        throw new ConnectionException("service not available");
    }
}
