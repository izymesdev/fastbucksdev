package com.izymes.jira.fastbucks.clients.xero;

import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.Maps;
import com.izymes.jira.fastbucks.api.FastbucksClient;
import com.izymes.jira.fastbucks.api.GenericInvoice;
import com.izymes.jira.fastbucks.api.GenericItem;
import com.izymes.jira.fastbucks.api.GenericResponse;
import com.izymes.jira.fastbucks.clients.xero.generated.*;
import com.izymes.jira.fastbucks.clients.xero.generated.ArrayOfLineItem;
import com.izymes.jira.fastbucks.clients.xero.generated.Contact;
import com.izymes.jira.fastbucks.clients.xero.generated.Invoice;
import com.izymes.jira.fastbucks.clients.xero.generated.InvoiceType;
import com.izymes.jira.fastbucks.clients.xero.generated.LineItem;
import com.izymes.jira.fastbucks.clients.xero.generated.ResponseType;
import net.oauth.OAuthException;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

public class XeroClient implements FastbucksClient {
    private List<String> configKeys;
    private Map<String, String> configMap;
    public final String name="Xero";
    public static final long THIRTY_DAYS_MS = 30L * 24L * 60L * 60L * 1000L;

    private final I18nResolver i18nResolver;

    private static final Logger log = Logger.getLogger( XeroClient.class );

    public static enum XeroEndpoint{
        Organisation,
        Contacts,
        Invoices;
    };

    public XeroClient(I18nResolver i18nResolver) {
        this.i18nResolver = i18nResolver;
    }

    public static XeroEndpoint DEFAULT_ENDPOINT = XeroEndpoint.Organisation;

    public void setConfig(Map<String, String> configMap) {
        this.configMap = configMap;
    }

    public Map<String, String> getConfig() {
        return configMap;
    }

    public List<String> getConfigKeys() {
        return this.configKeys;
    }

    public void setConfigKeys(List<String> configKeys) {
        this.configKeys = configKeys;
    }

    public GenericResponse sendInvoice(GenericInvoice genericInvoice) {
        Invoice invoice = new Invoice();
        invoice.setType( InvoiceType.ACCREC );
        Date date = new Date();
        invoice.setDate(getXmlGregorianCalendar(date));
        invoice.setDueDate(getXmlGregorianCalendar(new Date(date.getTime() + THIRTY_DAYS_MS)));
        invoice.setInvoiceNumber(genericInvoice.getInvoiceNumber());
        invoice.getLineAmountTypes().add("Inclusive");
        invoice.setContact( getContact( genericInvoice.getContactName() ) );
        ArrayOfLineItem lineItems = new ArrayOfLineItem();

        for ( GenericItem item : genericInvoice.getItems()){
            LineItem lineItem = new LineItem();
            lineItem.setDescription(String.format("%s %s",item.getName() , item.getDescription() ) );
            lineItem.setQuantity(new BigDecimal(item.getQuantity()));
            lineItem.setUnitAmount( new BigDecimal( item.getUnitCost() ));
            lineItem.setAccountCode( "200" );
            lineItem.setTaxType( "OUTPUT" );
            lineItems.getLineItem().add( lineItem );
        }

        invoice.setLineItems( lineItems );
        return sendInvoice( invoice );

    }

    public GenericResponse testConnection( TEST_ENDPOINT endPoint, Object... args) {
        String testEndPoint = DEFAULT_ENDPOINT.name();
        String query = "";
        String contactName = "";
        if ( endPoint == TEST_ENDPOINT.contact && args.length > 0 ){
            contactName = (String) args[0];
            query = "Name==\""+ contactName + "\"";
            testEndPoint = XeroEndpoint.Contacts.name();
        }

        String encodedQuery = "";
        try {
            encodedQuery = URLEncoder.encode(query, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        String uri = configMap.get("com.izymes.jira.fastbucks.client.xero.url") + testEndPoint + "?where=" + encodedQuery;

        com.izymes.jira.fastbucks.clients.xero.generated.ResponseType response = null;
        GenericResponse testResponse = new GenericResponse();
        try {
            XeroConnection xc = new XeroConnectionImpl( configMap );
            xc.setUri( uri );
            response = xc.get();
        } catch (IOException e) {
            e.printStackTrace();
            testResponse.setException( e );
        } catch (OAuthException e) {
            e.printStackTrace();
            testResponse.setException( e );
        } catch (URISyntaxException e) {
            e.printStackTrace();
            testResponse.setException( e );
        }

        if( testResponse.getException() != null ){
            return testResponse;
        }

        if ( endPoint == TEST_ENDPOINT.contact ){
            List<Contact> contactList = response.getContacts().getContact();
            if ( contactList.size() > 0 ) {
                testResponse.addMessage(i18nResolver.getText("fastbucks.project.config.client.test.contact", 1, contactName, contactList.get(0).getEmailAddress(), response.getStatus() ) );
            }
            return testResponse;
        }
        testResponse.addMessage(i18nResolver.getText("fastbucks.project.config.client.test.organisation", response.getOrganisations().getOrganisation().get(0).getName()));
        testResponse.addMessage(  "\n" + response.getStatus() );
        return testResponse ;
    }




    public String getName() {
        return name;
    }

    private GenericResponse sendInvoice(Invoice invoice) {
        String endPoint = XeroEndpoint.Invoices.name();
        String encInvoiceNumber = "";
        try {
             encInvoiceNumber = URLEncoder.encode( invoice.getInvoiceNumber(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        String uri = configMap.get("com.izymes.jira.fastbucks.client.xero.url") + endPoint + "/" + encInvoiceNumber;
        ResponseType response = null;
        XeroConnection xc = new XeroConnectionImpl(configMap);
        GenericResponse genericResponse = new GenericResponse();
        try {
            response = xc.post( uri, getFormParams("Invoice", invoice) );
        } catch (IOException e) {
            log.error("", e);
            genericResponse.setException( e );
        } catch (URISyntaxException e) {
            log.error("", e);
            genericResponse.setException(e);
        } catch (OAuthException e) {
            log.error("", e);
            genericResponse.setException( e );
        }
        if ( genericResponse.getException() != null ){
            return genericResponse;
        }

        GenericInvoice invoiceResponse = new GenericInvoice();
        Invoice sentInvoice = response.getInvoices().getInvoice().get(0);
        invoiceResponse.setInvoiceNumber(sentInvoice.getInvoiceNumber());
        invoiceResponse.setContactName( sentInvoice.getContact().getName() );
        genericResponse.setConfirmedInvoice(invoiceResponse);
        return genericResponse;

    }

    private Map<String,String> getFormParams(String qName, Object data) {
        ByteArrayOutputStream os = null;
        try {
            os = XmlUtils.getInstance().generateXml( data, qName );
        } catch (JAXBException e) {
            log.error("can't generate invoice xml", e);
            return null;
        }
        String value = os.toString();

        //xero does not like empty line amount elements
        String hackValue = value.replaceAll("<LineAmount xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:nil=\"true\"/>", "");

        Map<String, String> params = Maps.newHashMap();
        params.put("xml", hackValue );
        return params;
    }

    private Contact getContact(String contactName) {
        String query = "Name==\"" + contactName + "\"";
        String endPoint = "Contacts";
        String encodedQuery = "";
        try {
            encodedQuery = URLEncoder.encode(query, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }
        String uri = configMap.get("com.izymes.jira.fastbucks.client.xero.url") + endPoint + "?where=" + encodedQuery;

        ResponseType response = null;
        XeroConnection xc = new XeroConnectionImpl(configMap);
        xc.setUri(uri);
        try {
            response = xc.get();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        } catch (URISyntaxException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        } catch (OAuthException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }
        if ( response.getContacts() == null || response.getContacts().getContact().size() == 0 ){
            log.error( "no contacts found");
            return null;
        }
        if ( response.getContacts().getContact().size() > 1 ){
            log.error( "more than one contact matching " + contactName + " found. returning first found");
            return response.getContacts().getContact().get(0);
        }

        return  response.getContacts().getContact().get(0);
      }


    private XMLGregorianCalendar getXmlGregorianCalendar(Date date) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis( date.getTime() );
        DatatypeFactory df = null;
        try {
            df = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if ( df != null ) {
            return df.newXMLGregorianCalendar(gc);
        }
        return null;
    }


}
