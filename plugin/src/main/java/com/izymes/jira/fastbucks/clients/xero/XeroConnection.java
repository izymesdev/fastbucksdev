package com.izymes.jira.fastbucks.clients.xero;

import com.izymes.jira.fastbucks.clients.xero.generated.ResponseType;
import net.oauth.OAuthException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;


public interface XeroConnection {
    @Deprecated
    XeroConnection setUri(String uri);
    @Deprecated
    ResponseType get() throws IOException, URISyntaxException, OAuthException;

    ResponseType get(String path) throws IOException, URISyntaxException, OAuthException;
    ResponseType post(String path, Map<String, String> params) throws IOException, URISyntaxException, OAuthException;
}
