package com.izymes.jira.fastbucks.job;


import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.worklog.WorklogService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.izymes.jira.fastbucks.api.FastbucksClient;
import com.izymes.jira.fastbucks.api.GenericInvoice;
import com.izymes.jira.fastbucks.api.GenericResponse;
import com.izymes.jira.fastbucks.model.CommonProjectConfig;
import com.izymes.jira.fastbucks.modules.*;
import com.izymes.jira.fastbucks.util.InvoiceBuilder;
import com.opensymphony.util.TextUtils;
import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobExecutionException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class ProjectJob implements PluginJob {

    private ProjectConfigManager projectConfigManager;
    private ClientManager clientManager;
    private SearchService searchService;
    private UserManager userManager;
    private WorklogService worklogService;
    private ProjectManager projectManager;
    public static final String KEY = ProjectJob.class.getSimpleName() + ":";

    private static Logger log = Logger.getLogger( ProjectJob.class );
    private WorklogScheduler worklogScheduler;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yy");

    @Override
    public void execute(Map<String, Object> jobDataMap) {
        JobContainer jobContainer = (JobContainer)jobDataMap.get( JobContainer.KEY );
        setComponents( jobContainer );
        worklogScheduler = (DefaultWorklogScheduler)jobDataMap.get("scheduler");
        String projectKey = (String)jobDataMap.get("projectkey");
        CronTrigger cronTrigger = (CronTrigger)jobDataMap.get("crontrigger");
        try {
            cronTrigger.triggered(null);
            cronTrigger.setPreviousFireTime( (Date)jobDataMap.get("lastrun") ); // the time when the job was scheduled
            execute(projectKey, cronTrigger);
            worklogScheduler.updateScheduler(projectKey, new Date());
        } catch (JobExecutionException e) {
            log.error( "Not rescheduling project " + projectKey, e );
            WorklogScheduler worklogScheduler = (DefaultWorklogScheduler)jobDataMap.get("scheduler");
            worklogScheduler.resetScheduler( projectKey );
        }
    }

    private void execute( String projectKey, CronTrigger trigger  ) throws JobExecutionException {
        CommonProjectConfig projectConfig = projectConfigManager.getCommonProjectConfig(projectKey);
        String username = projectConfig.getUsername();
        Project project = projectManager.getProjectObjByKey(projectKey);
        if ( project == null ){
            // project deleted or something happened
            throw new JobExecutionException("Project " + projectKey + " not found - terminating schedule");
        }
        if ( !TextUtils.stringSet( username )){
            username = project.getLead().getName();
        }
        long startPeriod = calculateStartOfPeriod(trigger, projectConfig.getStartDate() );
        log.info( "Job: Crontrigger next firetime " + trigger.getNextFireTime() );
        log.info(" Job: search issues starting from " + startPeriod );

        final JqlQueryBuilder queryBuilder = JqlQueryBuilder.newBuilder();
        final Query query = queryBuilder.where().project(projectKey).and().updatedAfter(new Date(startPeriod)).buildQuery();

        try {
            User user = userManager.getUser(username);
            log.info( new Date() + " Job:execute for " + projectKey + ". Next run is @" + trigger.getNextFireTime());
            final SearchResults results = searchService.search(user, query, PagerFilter.getUnlimitedFilter());
            if ( results == null || results.getIssues().size() == 0 ){
                return;
            }
            final JiraServiceContext serviceContext = new JiraServiceContextImpl(user);
            final InvoiceBuilder invoiceBuilder = InvoiceBuilder.INSTANCE.create()
                    .setWorklogService(worklogService)
                    .setProjectConfig(projectKey, projectConfig)
                    .setJiraServiceContext( serviceContext );
            for(Issue issue : results.getIssues() ){
                invoiceBuilder.addIssueWorklog( issue, startPeriod );
            }
            GenericInvoice invoice = invoiceBuilder.build();
            if ( invoice.getItems().size() > 0){
                FastbucksClient fastbucksClient = clientManager.getClientByName(projectConfig.getProjectClientName());
                GenericResponse response = fastbucksClient.sendInvoice(invoice);
                if ( response.getException() != null ){
                    log.error("invoice not sent ", response.getException() );
                    return;
                }
                String invoiceNumber = response.getConfirmedInvoice().getInvoiceNumber();
                for(Issue issue : results.getIssues() ){
                    invoiceBuilder.tagIssueWorklog(invoiceNumber, issue);
                }
                log.info(String.format("Invoice %s for project %s sent to %s", invoiceNumber,projectKey,response.getConfirmedInvoice().getContactName()) );
            }

        } catch (SearchException e) {
            log.error("", e);
        }
    }

    private long calculateStartOfPeriod(CronTrigger trigger, String startDate) {
        if ( TextUtils.stringSet( startDate )){
            try {
                return dateFormat.parse( startDate ).getTime();
            } catch (ParseException e) {
                log.error("cannot parse start date", e);
            }
        }

        return trigger.getPreviousFireTime().getTime();
    }

    private void setComponents( JobContainer jobContainer  ){
        clientManager = jobContainer.getClientManager();
        projectConfigManager = jobContainer.getProjectConfigManager();
        userManager = jobContainer.getUserManager();
        searchService = jobContainer.getSearchService();
        worklogService = jobContainer.getWorklogService();
        projectManager = jobContainer.getProjectManager();
    }


}
