package com.izymes.jira.fastbucks.model;

import com.izymes.jira.fastbucks.job.ProjectJob;
import com.izymes.jira.fastbucks.modules.JobContainer;
import org.quartz.JobDetail;

public class FastbucksJobDetail extends JobDetail {

    private final JobContainer jobContainer;

    public FastbucksJobDetail(JobContainer jobContainer) {
        this.jobContainer = jobContainer;
        setJobClass(ProjectJob.class);
        setDurability(true);
    }

    public JobContainer getJobContainer() {
        return jobContainer;
    }
}
