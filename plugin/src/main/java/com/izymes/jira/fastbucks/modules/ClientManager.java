package com.izymes.jira.fastbucks.modules;

import com.izymes.jira.fastbucks.api.FastbucksClient;
import com.izymes.jira.fastbucks.api.GenericResponse;
import com.izymes.jira.fastbucks.model.ClientException;

import java.util.List;
import java.util.Map;


public interface ClientManager {
    public List<FastbucksClient> detectClients();
    public GenericResponse testConnection( String clientName );
    public GenericResponse testProjectSettings( String projectKey );
    public List<FastbucksClient> getClients();
    public FastbucksClient getClientByName(final String clientName);
    public void setClientConfig(String clientName,  Map<String, String> clientConfig);
    public Map<String, String> getClientConfig(String clientName);
}
