package com.izymes.jira.fastbucks.modules;


import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.izymes.jira.fastbucks.api.FastbucksClient;
import com.izymes.jira.fastbucks.api.GenericResponse;
import com.izymes.jira.fastbucks.model.CommonProjectConfig;
import org.springframework.beans.factory.InitializingBean;

import java.util.List;
import java.util.Map;

public class DefaultClientManager implements ClientManager, InitializingBean {
    private final  PluginAccessor pluginAccessor;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final ProjectConfigManager projectConfigManager;
    private final List<FastbucksClient> clients = Lists.newLinkedList();

    public DefaultClientManager(PluginAccessor pluginAccessor, PluginSettingsFactory pluginSettingsFactory, ProjectConfigManager projectConfigManager) {
        this.pluginAccessor = pluginAccessor;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.projectConfigManager = projectConfigManager;
    }

    @Override
    public List<FastbucksClient> getClients() {

        if ( clients.size() == 0 ){
            detectClients();
        }

        return clients;
    }
    @Override
    public List<FastbucksClient> detectClients() {

        clients.clear();
        List<FastbucksClientModuleDescriptor> descriptors = pluginAccessor.getEnabledModuleDescriptorsByClass( FastbucksClientModuleDescriptor.class );
        for(FastbucksClientModuleDescriptor d : descriptors ){
            FastbucksClient fastbucksClient = d.getModule();
            fastbucksClient.setConfig( getClientConfig( fastbucksClient.getName() ));
            clients.add(fastbucksClient);
        }
        return clients;
    }

    @Override
    public GenericResponse testProjectSettings(String projectKey) {

        CommonProjectConfig projectConfig = projectConfigManager.getCommonProjectConfig( projectKey );
        FastbucksClient client = getClientByName( projectConfig.getProjectClientName() );
        return client.testConnection( FastbucksClient.TEST_ENDPOINT.contact, projectConfig.getBillingContact() );
    }

    @Override
    public GenericResponse testConnection(String clientName) {
        FastbucksClient client = getClientByName( clientName );
        return client.testConnection( FastbucksClient.TEST_ENDPOINT.connection );
    }

    @Override
    public void setClientConfig( final String clientName, Map<String, String> clientConfig) {

        FastbucksClient client = getClientByName(clientName);
        if ( client != null ){
            client.setConfig(clientConfig);
            PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
            pluginSettings.put( clientName , clientConfig );
        }

    }

    public FastbucksClient getClientByName(final String clientName) {
        return Iterables.find(getClients(), new Predicate<FastbucksClient>() {
            @Override
            public boolean apply(FastbucksClient fastbucksClient) {
                return clientName.equals(fastbucksClient.getName());
            }
        });
    }

    @Override
    public Map<String, String> getClientConfig(String clientName ) {
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        Map<String, String> clientConfig = (Map<String, String>) pluginSettings.get( clientName );
        return clientConfig;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        detectClients();
    }
}
