package com.izymes.jira.fastbucks.modules;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.upm.api.license.entity.PluginLicense;
import com.atlassian.upm.api.license.event.PluginLicenseChangeEvent;
import com.atlassian.upm.api.util.Option;
import com.atlassian.upm.license.storage.lib.PluginLicenseStoragePluginUnresolvedException;
import com.atlassian.upm.license.storage.lib.ThirdPartyPluginLicenseStorageManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.net.URI;
import java.net.URISyntaxException;

public class DefaultMarketPlaceLicenseManager implements MarketPlaceLicenseManager, InitializingBean, DisposableBean {
    private final static Logger log = Logger.getLogger(DefaultMarketPlaceLicenseManager.class);

    private final ThirdPartyPluginLicenseStorageManager licenseManager;
    private final ApplicationProperties applicationProperties;
    private final I18nResolver i18nResolver;

    private final EventPublisher eventPublisher;
    private  WorklogScheduler worklogScheduler;


    public DefaultMarketPlaceLicenseManager(ThirdPartyPluginLicenseStorageManager licenseManager, ApplicationProperties applicationProperties, I18nResolver i18nResolver, EventPublisher eventPublisher) {
        this.licenseManager = licenseManager;
        this.applicationProperties = applicationProperties;
        this.i18nResolver = i18nResolver;
        this.eventPublisher = eventPublisher;
    }

    @Override
    public String validateLicense() {
        Option<PluginLicense> license = null;
        URI licenseUrl = null;
        try {
            license = licenseManager.getLicense();
            if (licenseManager.isUpmLicensingAware()) {
                licenseUrl = licenseManager.getPluginManagementUri();
            } else {
                licenseUrl = new URI(applicationProperties.getBaseUrl() + "/plugins/servlet/com.izymes.fastbucks-plugin/license");
            }
        } catch (PluginLicenseStoragePluginUnresolvedException e) {
            return i18nResolver.getText("plugin.license.storage.admin.plugin.unavailable");
        } catch (URISyntaxException e) {
            log.error("", e);
            return "";
        }
        if (license.isDefined()) {
            PluginLicense pluginLicense = license.get();
            //Check and see if the stored license has an error. If not, it is currently valid.
            if (pluginLicense.getError().isDefined()) {
                return i18nResolver.getText("plugin.license.storage.admin.license.attribute.status.invalid.noneval", "", pluginLicense.getError().get().name());

            }
        } else {
            //No license (valid or invalid) is stored.
            String licenseLink = String.format("<a href='%s'>%s</a>", licenseUrl, i18nResolver.getText("plugin.license.storage.admin.title", ""));
            return i18nResolver.getText("plugin.license.storage.admin.license.attribute.status.unlicensed") +
                    " " + licenseLink;
        }

        return "";

    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }

    @EventListener
    public void onPluginLicenseChangeEvent(PluginLicenseChangeEvent event) {

        worklogScheduler = DefaultWorklogScheduler.getINSTANCE();

        try {
            PluginLicense license = event.getLicense();
            if ( event.getPluginKey().equals( licenseManager.getPluginKey() ) &&
                 license.isValid()  ) {
                worklogScheduler.onStart();
            }
        } catch (PluginLicenseStoragePluginUnresolvedException e) {
            log.error("", e);
        }
    }

}
