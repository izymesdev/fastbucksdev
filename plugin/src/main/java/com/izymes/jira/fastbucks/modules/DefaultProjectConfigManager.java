package com.izymes.jira.fastbucks.modules;

import com.atlassian.jira.web.component.cron.CronEditorBean;
import com.atlassian.jira.web.component.cron.CronEditorWebComponent;
import com.atlassian.jira.web.component.cron.parser.CronExpressionParser;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.izymes.jira.fastbucks.model.CommonProjectConfig;
import com.opensymphony.util.TextUtils;
import org.apache.log4j.Logger;
import org.quartz.CronTrigger;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DefaultProjectConfigManager implements ProjectConfigManager {

    private final static Logger log = Logger.getLogger( DefaultProjectConfigManager.class);
    private final PluginSettingsFactory pluginSettingsFactory;
    private final MarketPlaceLicenseManager marketPlaceLicenseManager;

    private final String PROJECT_KEY = "fastbucks.project";
    private final String GLOBAL_PROJECT_KEYS = "fastbucks.global.projectkeys";
    public static final String CRON_EDITOR_HTML_SCRIPT_PATTERN = "<script.*</script>";

    public enum projectConfigKeys {
        cronexpression,
        startdate,
        projectrate,
//        projectuser,
        billingcontact,
        projectclient
    };

    public DefaultProjectConfigManager(PluginSettingsFactory pluginSettingsFactory, MarketPlaceLicenseManager marketPlaceLicenseManager) {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.marketPlaceLicenseManager = marketPlaceLicenseManager;
    }

    @Override
    public String validate(Map<String, Object>[] projectConfigArray) {
        StringBuilder errMsg = new StringBuilder();
        errMsg.append( validateSize("more than one config set", 1, projectConfigArray ) )
                .append( validateCron("cron expression not valid - not scheduling", projectConfigArray[0] ) )
                .append( marketPlaceLicenseManager.validateLicense());
        return errMsg.toString();
    }

    @Override
    public String getCronEditorHtml(String key) {
        CommonProjectConfig projectConfig = getCommonProjectConfig(key);
        String cron = projectConfig.getCronExpression();
        if ( !TextUtils.stringSet(cron) ){
            cron = "0 0 23 L * ?";
        }
        CronExpressionParser cronExpressionParser = new CronExpressionParser(cron);
        CronEditorBean cronEditorBean = cronExpressionParser.getCronEditorBean();
        CronEditorWebComponent component = new CronEditorWebComponent();
        String html = component.getHtml(cronEditorBean, "fastbucks");
        Matcher m = Pattern.compile(CRON_EDITOR_HTML_SCRIPT_PATTERN, Pattern.DOTALL).matcher(html);
        m.find();
        String plainHtml = m.replaceAll("");
        return plainHtml;

    }

    private String validateCron(String msg, Map<String, Object> projectConfigMap) {
        String cron = (String)projectConfigMap.get( projectConfigKeys.cronexpression.name() );
        if ( !TextUtils.stringSet( cron )){
            return msg;
        }
        CronTrigger testCron = new CronTrigger();
        try {
            testCron.setCronExpression( cron );
        } catch (ParseException e) {
            return msg;
        }
        return "";
    }

    private String validateSize(String msg, int i, Map<String, Object>[] projectConfigArray) {
        if( projectConfigArray.length > i ){
            return msg;
        }
         return "";
    }


    @Override
    public void setCommonProjectConfig(String projectKey, Map<String, Object> config) {
        PluginSettings pluginSettings = pluginSettingsFactory.createSettingsForKey( projectKey );
        pluginSettings.put(PROJECT_KEY, config);
        updateStoredProjects( projectKey );
    }

    private void updateStoredProjects(String projectKey) {
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        Set<String> projectKeys;
        List<String> projectKeyList = (List<String>) pluginSettings.get(GLOBAL_PROJECT_KEYS);
        if( projectKeyList == null ){
            projectKeys = Sets.newHashSet();
        } else {
            projectKeys = Sets.newHashSet( projectKeyList);
        }
        projectKeys.add( projectKey );
        pluginSettings.put( GLOBAL_PROJECT_KEYS, Lists.newArrayList( projectKeys ) );
    }

    @Override
    public CommonProjectConfig getCommonProjectConfig(String projectKey) {
        PluginSettings pluginSettings = pluginSettingsFactory.createSettingsForKey( projectKey );
        CommonProjectConfig config = new CommonProjectConfig();
        Map<String, String> configMap = (Map<String, String>) pluginSettings.get( PROJECT_KEY );
        if ( configMap == null ){
            return config;
        }
        config.setCronExpression( configMap.get(projectConfigKeys.cronexpression.name()));
        config.setStartDate( configMap.get(projectConfigKeys.startdate.name()));
        try {
            String projectrate = configMap.get(projectConfigKeys.projectrate.name());
            if (TextUtils.stringSet( projectrate ))
                config.setProjectRate( Float.parseFloat(projectrate));
        } catch (NumberFormatException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
//        config.setUsername( configMap.get("projectuser"));
        config.setUsername( "" );
        config.setBillingContact( configMap.get(projectConfigKeys.billingcontact.name()));
        config.setProjectClientName(configMap.get(projectConfigKeys.projectclient.name()));
        return config;
    }

    @Override
    public List<String> getScheduledProjects() {
        PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
        List<String> projectKeys = (List<String>) pluginSettings.get( GLOBAL_PROJECT_KEYS );

        if ( projectKeys == null ){
            return Lists.newArrayList();
        }
        return projectKeys;
    }

}
