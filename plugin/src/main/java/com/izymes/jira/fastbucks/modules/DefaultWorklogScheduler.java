package com.izymes.jira.fastbucks.modules;


import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.google.common.collect.Maps;
import com.izymes.jira.fastbucks.job.ProjectJob;
import com.izymes.jira.fastbucks.model.CommonProjectConfig;
import com.opensymphony.util.TextUtils;
import org.apache.log4j.Logger;
import org.quartz.CronTrigger;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

public class DefaultWorklogScheduler implements WorklogScheduler, LifecycleAware {

    public final static String PROJECT_SCHEDULER_LAST_RUN_KEY = "fastbucks.project.scheduler.lastrun";

    private static final Logger log = Logger.getLogger( DefaultWorklogScheduler.class );
    private static DefaultWorklogScheduler INSTANCE;

    private final ProjectConfigManager projectConfigManager;
    private final JobContainer jobContainer;
    private final PluginScheduler pluginScheduler; // provided by SAL
    private final MarketPlaceLicenseManager marketPlaceLicenseManager;
    private final PluginSettingsFactory pluginSettingsFactory;



    public DefaultWorklogScheduler(ProjectConfigManager projectConfigManager, JobContainer jobContainer, PluginScheduler pluginScheduler, MarketPlaceLicenseManager marketPlaceLicenseManager, PluginSettingsFactory pluginSettingsFactory) {
        this.projectConfigManager = projectConfigManager;
        this.jobContainer = jobContainer;
        this.pluginScheduler = pluginScheduler;
        this.marketPlaceLicenseManager = marketPlaceLicenseManager;
        this.pluginSettingsFactory = pluginSettingsFactory;
        INSTANCE = this;
    }

    public static DefaultWorklogScheduler getINSTANCE() {
        return INSTANCE;
    }

    @Override
    public void resetScheduler(String projectKey) {
        String jobKey = ProjectJob.KEY + projectKey;
        log.warn("stopping scheduler for project " + projectKey);
        try {
            pluginScheduler.unscheduleJob( jobKey );
        } catch (IllegalArgumentException e) {
            log.warn("Job " + jobKey + " doesn't exist, cannot unschedule");
        }
    }

    @Override
    public void setScheduler(String projectKey) {
        Long lastRun = new SchedulerProcessor(){
            @Override
            long process(String projectKey, Long timeStamp) {
                PluginSettings pluginSettings = pluginSettingsFactory.createSettingsForKey( projectKey );
                Object time = pluginSettings.get( PROJECT_SCHEDULER_LAST_RUN_KEY );
                if ( time == null ){
                    time = new Date().getTime();
                }
                pluginSettings.put( PROJECT_SCHEDULER_LAST_RUN_KEY, time+"" );
                return Long.parseLong(time+"");
            }
        }.process(projectKey, null);
        updateScheduler( projectKey, new Date(lastRun) );
    }

    @Override
    public void updateScheduler(String projectKey, Date lastRun) {

        String jobKey = ProjectJob.KEY + projectKey;
        try {
            pluginScheduler.unscheduleJob( jobKey );
        } catch (IllegalArgumentException e) {
            log.warn("Job " + jobKey + " doesn't exist, cannot unschedule");
        }

        String licenseMessage = marketPlaceLicenseManager.validateLicense();
        if (TextUtils.stringSet(licenseMessage) ){
            log.error( "invalid license - not scheduling : " + licenseMessage );
            return;
        }

        CommonProjectConfig config = projectConfigManager.getCommonProjectConfig(projectKey);
        CronTrigger cronTrigger = getCronTrigger( projectKey );
        try {
            cronTrigger.setCronExpression( config.getCronExpression() );
        } catch (ParseException e) {
            log.error("invalid cron expression - not scheduling", e);
            return;
        }

        new SchedulerProcessor(){
            @Override
            long process(String projectKey, Long timeStamp) {
                PluginSettings pluginSettings = pluginSettingsFactory.createSettingsForKey( projectKey );
                pluginSettings.put(PROJECT_SCHEDULER_LAST_RUN_KEY, timeStamp+"" );
                return timeStamp;
            }
        }.process(projectKey, lastRun.getTime());

        Map<String, Object> jobDataMap = Maps.newHashMap();
        jobDataMap.put(JobContainer.KEY, jobContainer );
        jobDataMap.put("projectkey", projectKey);
        jobDataMap.put("crontrigger" , cronTrigger );
        jobDataMap.put("lastrun", lastRun);
        jobDataMap.put("scheduler", this );
        cronTrigger.triggered( null );                // sync to now
        Date startTime = cronTrigger.getNextFireTime();   // start when next due
        long delay = cronTrigger.getFireTimeAfter( startTime ).getTime() - startTime.getTime();
        log.info(String.format("Scheduler : Job %s scheduled at %s executing every %s ms", jobKey, startTime, delay));
        pluginScheduler.scheduleJob(jobKey,
                ProjectJob.class,
                jobDataMap,
                startTime,
                delay);

    }

    private CronTrigger getCronTrigger(String projectKey) {
        CronTrigger cronTrigger = new CronTrigger();
        cronTrigger.setName(projectKey);
        return cronTrigger;
    }

    abstract class SchedulerProcessor{
        abstract long process(String key, Long timeStamp);

    }


    @Override
    public void onStart() {
        for( String key : projectConfigManager.getScheduledProjects() ){
            setScheduler(key);
        }
    }


}
