package com.izymes.jira.fastbucks.modules;

import com.atlassian.plugin.Plugin;
import com.google.common.collect.Lists;
import org.dom4j.Element;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.izymes.jira.fastbucks.api.FastbucksClient;

import java.util.List;

public class FastbucksClientModuleDescriptor extends AbstractModuleDescriptor<FastbucksClient>{
    private final List<String> params = Lists.newArrayList();

    public FastbucksClientModuleDescriptor(ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    @Override
    public void init(Plugin plugin, Element element) throws PluginParseException
    {
        super.init(plugin, element);
        String paramKeys = element.getData().toString();
        for(String s : paramKeys.split(",")){
            params.add(s.trim());
        }
    }

    public FastbucksClient getModule()
    {
        FastbucksClient client = moduleFactory.createModule(moduleClassName, this);
        client.setConfigKeys( params );
        return client;
    }

}