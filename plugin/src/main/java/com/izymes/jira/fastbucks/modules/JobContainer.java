package com.izymes.jira.fastbucks.modules;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.worklog.WorklogService;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.util.UserManager;

/**
 * Spring container helper
 */
public class JobContainer {

    private final ProjectConfigManager projectConfigManager;
    private final ClientManager clientManager;
    private final UserManager userManager;
    private final SearchService searchService;
    private final WorklogService worklogService;
    private final ProjectManager projectManager;
    public static String KEY = JobContainer.class.getName();

    public JobContainer(ProjectConfigManager projectConfigManager, ClientManager clientManager,
                        UserManager userManager, SearchService searchService, WorklogService worklogService, ProjectManager projectManager) {
        this.projectConfigManager = projectConfigManager;
        this.clientManager = clientManager;
        this.userManager = userManager;
        this.searchService = searchService;
        this.worklogService = worklogService;
        this.projectManager = projectManager;
    }

    public ProjectConfigManager getProjectConfigManager() {
        return projectConfigManager;
    }

    public ClientManager getClientManager() {
        return clientManager;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public SearchService getSearchService() {
        return searchService;
    }

    public WorklogService getWorklogService() {
        return worklogService;
    }

    public ProjectManager getProjectManager() {
        return projectManager;
    }
}
