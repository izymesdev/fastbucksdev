package com.izymes.jira.fastbucks.modules;

public interface MarketPlaceLicenseManager {
    public String validateLicense();
}
