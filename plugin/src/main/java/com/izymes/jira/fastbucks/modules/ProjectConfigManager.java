package com.izymes.jira.fastbucks.modules;


import com.izymes.jira.fastbucks.model.CommonProjectConfig;

import java.util.List;
import java.util.Map;

public interface ProjectConfigManager {

    public void setCommonProjectConfig(String projectKey, Map<String, Object> config);
    public CommonProjectConfig getCommonProjectConfig(String projectKey);
    public List<String> getScheduledProjects();
    public String validate(Map<String, Object>[] projectConfigArray);

    public String getCronEditorHtml(String key);
}
