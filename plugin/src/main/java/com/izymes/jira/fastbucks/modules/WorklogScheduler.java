package com.izymes.jira.fastbucks.modules;


import com.atlassian.sal.api.lifecycle.LifecycleAware;

import java.util.Date;

public interface WorklogScheduler extends LifecycleAware{
    void updateScheduler(String projectKey, Date lastRun);
    void setScheduler(String projectKey);
    void resetScheduler(String projectKey );
}
