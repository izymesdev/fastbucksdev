package com.izymes.jira.fastbucks.rest;

import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.collect.Maps;
import com.izymes.jira.fastbucks.api.GenericResponse;
import com.izymes.jira.fastbucks.modules.ClientManager;
import org.codehaus.jackson.map.ObjectMapper;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.util.Map;

@Path("/client")
public class ClientConfigResources {

    private ClientManager clientManager;

    public ClientConfigResources( ClientManager clientManager ) {
        this.clientManager = clientManager;
    }

    @POST
    @Path("/{clientName}/{projectKey}/config")
    @Produces({MediaType.APPLICATION_JSON})
    public Response configClient(@PathParam("clientName") final String clientName,
                                          @PathParam("projectKey") final String projectKey,
                                          final Map<String, Object>[] clientConfigArray ){
        Map<String, String> clientConfig= Maps.newHashMap();
        for( Map<String, Object> el : clientConfigArray ){
            clientConfig.put((String)el.get("key"), (String)el.get("value"));
        }
        clientManager.setClientConfig( clientName,  clientConfig );

        return Response.ok().build();
    }

    @GET
    @Path("/{clientName}/test")
    @Produces( MediaType.APPLICATION_JSON)
    public Response testClientConnection(@PathParam("clientName") final String clientName){
        GenericResponse response =  clientManager.testConnection(clientName);
        if ( response.getException() != null ){
            return Response.serverError().entity( response.getException().getMessage() ).build();
        }

        return Response.ok( new TestResponse( response.getMessage() ) ).build();
    }

    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
    class TestResponse {

        @XmlElement(name="msg")
        String message;

        TestResponse(String message) {
            this.message = message;
        }
    }

}
