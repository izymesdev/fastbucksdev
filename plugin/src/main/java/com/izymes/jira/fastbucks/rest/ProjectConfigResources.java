package com.izymes.jira.fastbucks.rest;

import com.atlassian.jira.web.component.cron.CronEditorBean;
import com.atlassian.jira.web.component.cron.CronEditorWebComponent;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.izymes.jira.fastbucks.api.GenericResponse;
import com.izymes.jira.fastbucks.model.CommonProjectConfig;
import com.izymes.jira.fastbucks.modules.ClientManager;
import com.izymes.jira.fastbucks.modules.ProjectConfigManager;
import com.izymes.jira.fastbucks.modules.WorklogScheduler;
import com.opensymphony.util.TextUtils;
import webwork.action.ActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

@Path("/project")
public class ProjectConfigResources {
    private final ProjectConfigManager projectConfigManager;
    private final ClientManager clientManager;
    private final WorklogScheduler worklogScheduler;

    public ProjectConfigResources(ProjectConfigManager projectConfigManager, ClientManager clientManager, WorklogScheduler worklogScheduler) {
        this.projectConfigManager = projectConfigManager;
        this.clientManager = clientManager;
        this.worklogScheduler = worklogScheduler;
    }

    @POST
    @Path("/{projectId}/{projectKey}/config")
    @Produces({MediaType.APPLICATION_JSON})
    public Response configProjectSettings(@PathParam("projectId") final long projectId,
                                          @PathParam("projectKey") final String projectKey,
                                          final Map<String, Object>[] projectConfigArray){

        String errorMsg = projectConfigManager.validate( projectConfigArray );
        if (TextUtils.stringSet( errorMsg ) )
            return Response.serverError().entity(errorMsg).build();

        projectConfigManager.setCommonProjectConfig( projectKey, projectConfigArray[0] );
        worklogScheduler.setScheduler( projectKey );

        return Response.ok().build();
    }

    @POST
    @Path("/{projectKey}/cron")
    @Produces(MediaType.APPLICATION_JSON)
    public Response setCron(@Context HttpServletRequest request,
                            @PathParam("projectKey") final String projectKey
    ) {
        Map params = request.getParameterMap();
        CronEditorBean cronEditorBean = new CronEditorBean("fastbucks", params);
        CronEditorWebComponent component = new CronEditorWebComponent();
        String cron = component.getCronExpressionFromInput(cronEditorBean);
        TestResponse response = new TestResponse();
        response.setMessage( cron );
        return Response.ok(response).build();

    }

    @GET
    @Path("/{projectKey}/test")
    @Produces({MediaType.APPLICATION_JSON})
    public Response testProjectSettings( @PathParam("projectKey") final String projectKey ){
        TestResponse response = new TestResponse();
        GenericResponse clientResponse = clientManager.testProjectSettings(projectKey);
        if ( clientResponse.getException() != null ){
            return Response.serverError().entity( clientResponse.getException().getMessage()).build();
        }
        response.setMessage( clientResponse.getMessage() );
        return Response.ok( response ).build();
    }


    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
    class TestResponse {

        @XmlElement(name="msg")
        String message;

        public void setMessage(String message) {
            this.message = message;
        }
    }

}
