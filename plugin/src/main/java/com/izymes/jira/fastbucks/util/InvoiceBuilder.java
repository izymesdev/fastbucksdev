package com.izymes.jira.fastbucks.util;

import com.atlassian.core.util.DateUtils;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.issue.worklog.WorklogInputParameters;
import com.atlassian.jira.bc.issue.worklog.WorklogInputParametersImpl;
import com.atlassian.jira.bc.issue.worklog.WorklogResult;
import com.atlassian.jira.bc.issue.worklog.WorklogService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.worklog.Worklog;
import com.izymes.jira.fastbucks.api.GenericInvoice;
import com.izymes.jira.fastbucks.model.CommonProjectConfig;
import com.opensymphony.util.TextUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.FastDateFormat;

import java.util.Date;
import java.util.List;

public enum InvoiceBuilder {
    INSTANCE;


    private CommonProjectConfig projectConfig;
    private String invoiceNumber;
    private GenericInvoice genericInvoice;
    private WorklogService worklogService;
    private JiraServiceContext jiraServiceContext;
    public static final String INVOICE_PREFIX = "INV-";
    public static final String INVOICE_TAG_PREFIX = "T-";


    public InvoiceBuilder create() {
        genericInvoice = new GenericInvoice();
        return this;
    }

    public InvoiceBuilder setJiraServiceContext(JiraServiceContext jiraServiceContext) {
        this.jiraServiceContext = jiraServiceContext;
        return this;
    }

    public InvoiceBuilder setWorklogService(WorklogService worklogService) {
        this.worklogService = worklogService;
        return this;
    }

    public GenericInvoice build() {
        genericInvoice.setDate( new Date() );
        genericInvoice.setContactName( projectConfig.getBillingContact() );
        genericInvoice.setContactEmail( projectConfig.getBillingContact() );
        genericInvoice.setInvoiceNumber( this.invoiceNumber );
        return genericInvoice;
    }

    public InvoiceBuilder setProjectConfig(String projectKey, CommonProjectConfig projectConfig) {
        this.projectConfig = projectConfig;
        FastDateFormat df = FastDateFormat.getInstance("yyyy-MM-dd-HH-mm");
        this.invoiceNumber = INVOICE_PREFIX + projectKey + "-" + df.format(System.currentTimeMillis());
        return this;
    }

    public InvoiceBuilder addIssueWorklog(Issue issue, long startTime) {
        List<Worklog> worklogs = worklogService.getByIssue(jiraServiceContext, issue);
        long lastUpdated = issue.getUpdated().getTime();
        if (worklogs.size() > 0) {
            long timeSpent = 0L;
            for (Worklog w : worklogs) {
                if (!isTagged(w) && w.getUpdated().getTime() >= startTime ) {
                    timeSpent += w.getTimeSpent();
                }
            }
            if ( timeSpent > 0){
                float timeInHours = (float)timeSpent / (float)3600;
                genericInvoice.addItem(new Date(lastUpdated), issue.getKey(), issue.getSummary(), timeInHours, projectConfig.getProjectRate() );
            }
        }
        return this;
    }

    public void tagIssueWorklog(final String invoiceNumber, Issue issue ){
        List<Worklog> worklogs = worklogService.getByIssue(jiraServiceContext, issue);
        if (worklogs.size() > 0) {
            for (Worklog w : worklogs) {
                if ( !isTagged( w )){
                    updateWorklogEntry( invoiceNumber, w );
                }
            }
        }
    }

    private boolean isTagged(Worklog worklogEntry) {
            return worklogEntry.getComment().contains(INVOICE_TAG_PREFIX+INVOICE_PREFIX);
        }

    private void updateWorklogEntry( String invoiceNumber, Worklog worklogEntry) {
        if ( invoiceNumber.startsWith(INVOICE_PREFIX) ) {
            invoiceNumber = invoiceNumber.substring(INVOICE_PREFIX.length());
        }
        final WorklogInputParametersImpl.Builder builder = WorklogInputParametersImpl
                .timeSpent(DateUtils.getDurationString(worklogEntry.getTimeSpent()))
                .worklogId(worklogEntry.getId())
                .startDate(worklogEntry.getStartDate())
                .comment(worklogEntry.getComment() + String.format("\n T-INV-%s", invoiceNumber));  //tag the entry
        if ( TextUtils.stringSet(worklogEntry.getGroupLevel()) )
                builder.groupLevel(worklogEntry.getGroupLevel());
        if ( worklogEntry.getRoleLevelId() != null )
                builder.roleLevelId(worklogEntry.getRoleLevelId().toString());

        final WorklogInputParameters params = builder.build();
        WorklogResult worklogResult = worklogService.validateUpdate(jiraServiceContext, params);
        worklogService.updateAndRetainRemainingEstimate( jiraServiceContext, worklogResult, false );
    }

}
