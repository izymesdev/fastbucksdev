package com.izymes.jira.fastbucks.web;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.izymes.jira.fastbucks.model.CommonProjectConfig;
import com.izymes.jira.fastbucks.modules.ClientManager;
import com.izymes.jira.fastbucks.modules.ProjectConfigManager;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class ConfigContextProvider extends AbstractJiraContextProvider {
    private final ClientManager clientManager;
    private final ProjectConfigManager projectConfigManager;

    public ConfigContextProvider(  ClientManager clientManager, ProjectConfigManager projectConfigManager) {
        this.clientManager = clientManager;
        this.projectConfigManager = projectConfigManager;
    }

    @Override
    public Map getContextMap(User user, JiraHelper jiraHelper) {

        HttpServletRequest request = ExecutingHttpRequest.get();


        String projectContext = request.getRequestURI().split("project-config/")[1];
        String key = projectContext.split("/")[0];
        CommonProjectConfig config = projectConfigManager.getCommonProjectConfig( key );
        return EasyMap.build("clientList", clientManager.getClients(),
                "config", config,
                "req", request,
                "cronEditorHtml", projectConfigManager.getCronEditorHtml( key ));
    }
}
