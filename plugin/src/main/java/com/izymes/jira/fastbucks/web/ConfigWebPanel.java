package com.izymes.jira.fastbucks.web;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.web.model.WebPanel;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.izymes.jira.fastbucks.model.CommonProjectConfig;
import com.izymes.jira.fastbucks.modules.ClientManager;
import com.izymes.jira.fastbucks.modules.ProjectConfigManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class ConfigWebPanel implements WebPanel {
    private final ClientManager clientManager;
    private final ProjectConfigManager projectConfigManager;
    private static final String template = "/com/izymes/jira/fastbucks/vm/config.vm";
    private final TemplateRenderer templateRenderer;

    public ConfigWebPanel(ClientManager clientManager, ProjectConfigManager projectConfigManager, TemplateRenderer templateRenderer) {
        this.clientManager = clientManager;
        this.projectConfigManager = projectConfigManager;
        this.templateRenderer = templateRenderer;
    }

    public Map getContextMap(User user, JiraHelper jiraHelper) {

        HttpServletRequest request = ExecutingHttpRequest.get();


        String projectContext = request.getRequestURI().split("project-config/")[1];
        String key = projectContext.split("/")[0];
        CommonProjectConfig config = projectConfigManager.getCommonProjectConfig( key );
        return EasyMap.build("clientList", clientManager.getClients(),
                "config", config,
                "req", request,
                "cronEditorHtml", projectConfigManager.getCronEditorHtml( key ));
    }

    @Override
    public String getHtml(Map<String, Object> rendererContextMap) {
        HttpServletResponse response = ExecutingHttpRequest.getResponse();
        StringWriterResponse responseWriter = new StringWriterResponse(response );
        rendererContextMap.putAll(getContextMap(null, null));
        response.setContentType("text/html;charset=utf-8");
        try {
            templateRenderer.render(template, rendererContextMap, responseWriter.getWriter());
            return responseWriter.getContent();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return "";
    }

    class StringWriterResponse extends HttpServletResponseWrapper {

        private final CharArrayWriter charArrayWriter = new CharArrayWriter();
        /**
         * Constructs a request object wrapping the given request.
         *
         * @throws IllegalArgumentException if the request is null
         */
        public StringWriterResponse(HttpServletResponse request) {
            super(request);
        }

        public PrintWriter getWriter(){
            return new PrintWriter( charArrayWriter );
        }

        public String getContent(){
            return charArrayWriter.toString();
        }
    }
}
