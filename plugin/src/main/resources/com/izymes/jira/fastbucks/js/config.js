var Fastbucks = Fastbucks || {};
Fastbucks.spinner = Fastbucks.spinner || {};

var timesOnce = timesOnce || {};

(function ($) {

    var saveConfig = function (params, path, name, projectKey, $editLink, $paramSection) {
        var jsonData = JSON.stringify(params);
        console.log("client data " + jsonData);
        $.ajax({
            type:"POST",
            cache:false,
            contentType:"application/json; charset=utf-8",
            url:contextPath + "/rest/fastbucks/latest/" + path + "/" + name + "/" + projectKey + "/config",
            data:jsonData,
            success:function (data, textStatus, jqXHR) {
                $editLink.text(AJS.I18n.getText('fastbucks.project.config.edit'));
                $paramSection.slideToggle('slow');
            },
            error:function (jqXHR, textStatus, errorThrown) {
                $("#project-config-list-error").empty().html(jqXHR.responseText);
                setTimeout(function () {
                    $("#project-config-list-error").empty();
                }, 5000);
            }
        });
    }

    var getProjectParams = function ( $paramSection ) {

        var params = {};
        params.projectclient = AJS.escapeHtml($("#projectclient", $paramSection).val());
        params.projectrate = AJS.escapeHtml($("#projectrate", $paramSection).val());
        params.cronexpression = AJS.escapeHtml($("#project-config-scheduler-cron-field", $paramSection).val());
        params.billingcontact = AJS.escapeHtml($("#billingcontact", $paramSection).val());
        params.startdate = AJS.escapeHtml($("#project-config-scheduler-start-date-field").val());

        return params;

    }

    AJS.toInit(function ($) {
            AJS.log("init fastbucks");
            $("a.fastbucks-config").click(function (event) {
                event.preventDefault();
                var $editLink = $(this);
                var $paramSection = $(this).closest('span').next();
                if (!$paramSection.is(":visible")) {
                    $editLink.text(AJS.I18n.getText('fastbucks.project.config.save'));
                    $paramSection.slideToggle('slow');
                } else {
                    var path = "client";
                    var name = $(this).closest('li').find('.clientname').text();
                    var params = [];
                    var i = 0;
                    var projectKey = $("meta[name=projectKey]").attr("content");
                    var projectId = $("meta[name=projectId]").attr("content");
                    $(".fastbucks-client-config-param", $paramSection).each(function () {
                        params[i++] = { key:$(this).attr('id'), value: $(this).val() };
                    });
                    if ($(".fastbucks-project-config-param", $paramSection).length > 0) {
                        params[i++] = getProjectParams( $paramSection);
                        path = "project";
                        name = projectId;
                    }
                    saveConfig(params, path, name, projectKey, $editLink, $paramSection);
                }
            });

            var testConfig = function ($section, url) {
                $section.append("<div class='fastbucks-spinner'></div>");
                Fastbucks.spinner.start($section);
                $.ajax({url:url})
                    .done(function (data) {
                        Fastbucks.spinner.stop($section);
                        $section.append(AJS.template.load("client-result-template").fill({message:data.msg }));
                        setTimeout(function () {
                            $(".result-success", $section).hide('slow').remove();
                        }, 4000);
                    })
                    .fail(function (data, status, jqXHR) {
                        Fastbucks.spinner.stop($section);
                        $section.append(AJS.template.load("client-error-template").fill({message:data.responseText, status:data.status }));
                        setTimeout(function () {
                            $(".result-error", $section).hide('slow').remove();
                        }, 5000);

                    });
            };

            $("a.fastbucks-test-client").click(function (event) {
                event.preventDefault();
                var $paramSection = $(this).closest('span').next();
                var $client = $(this).closest('li').find('.clientname');
                var name = $client.text();
                var url = contextPath + "/rest/fastbucks/latest/client/" + name + "/test";
                var $clientSection = $client.parent();
                if ($paramSection.is(":visible")) {
                    $clientSection.append(AJS.template.load("client-result-template").fill({message:"Please save before test"}));
                    setTimeout(function () {
                        $(".result-success", $clientSection).hide('slow').remove();
                    }, 5000);
                    return;
                }
                testConfig($clientSection, url);
            });
            $("a.fastbucks-test-project").click(function (event) {
                event.preventDefault();
                var $paramSection = $(this).closest('span').next();
                var projectKey = $("meta[name=projectKey]").attr("content");
                var $projectSection = $(this).closest("li").find(".project-config-list-label");
                var url = contextPath + "/rest/fastbucks/latest/project/" + projectKey + "/test";
                if ($paramSection.is(":visible")) {
                    $projectSection.append(AJS.template.load("client-result-template").fill({message:"Please save before test"}));
                    setTimeout(function () {
                        $(".result-success", $projectSection).hide('slow').remove();
                    }, 5000);
                    return;
                }
                testConfig($projectSection, url);
            });

            $("#project-config-scheduler-cron-field-trigger").click( function(event ) {
                cronDialog.gotoPage(0);
                cronDialog.gotoPanel(0);
                cronDialog.show();

            });
            var cronDialog = new  AJS.Dialog({
                    width : 440,
                    height : 293,
                    id : "cron-dialog",
                    closeOnOutsideClick: true
                });

            cronDialog.addHeader(AJS.I18n.getText('fastbucks.project.config.cron.editor'));
            cronDialog.addPanel("Panel 1", "#fastbuckscroneditor", "panel-body");
            cronDialog.get("panel:0").setPadding(10);
            cronDialog.addButton("Done", function(dialog) {
                // dialog = This AJS.Dialog instance
                // page = The page of the dialog to which this button has been added
                // As of AUI 3.4:
                // Return true to allow the default action of this click event.
                // Return any other value to prevent the default action of this click event.
                var projectKey = $("meta[name=projectKey]").attr("content");
                var url = contextPath + "/rest/fastbucks/latest/project/" + projectKey + "/cron";
                $.post( url, $("form#fastbuckscroneditor").serialize())
                    .done( function( data ){
                        $("input#project-config-scheduler-cron-field").val( data.msg );
                        dialog.hide();
                        } )
                    .fail( function ( data ){
                        console.log( data );
                    } );
                return true;
                });
                cronDialog.addLink('cancel', function(dialog) {
                      // dialog = This AJS.Dialog instance
                      // page = The page of the dialog to which this button has been added
                      // As of AUI 3.4:
                      // Return true to allow the default action of this click event.
                      // Return any other value to prevent the default action of this click event.
                    dialog.hide();
                    });

            if (! timesOnce)
            {
                timesOnce = new Object();
            }
            timesOnce["fastbucks."] = true ;

//            methods from croneditor.js
            switchToDaysOfMonth('fastbucks.');
            switchToOnce('fastbucks.', false);

            console.log( $("#project-config-scheduler-start-date-field").html());
            Calendar.setup({
                        singleClick: true,
                        align: "Bl",
                        firstDay: AJS.params.firstDay,
                        button: "project-config-scheduler-start-date-trigger",
                        inputField: "project-config-scheduler-start-date-field",
                        currentMillis: AJS.params.currentMillis,
                        useISO8061: AJS.params.useISO8061,
                        ifFormat: AJS.params.dateFormat
                    });
        }
    );

    Fastbucks.spinner.start = function ( $element ) {
        Raphael.spinner($(".fastbucks-spinner", $element)[0], 8, "#666");
        $(".fastbucks-spinner", $element).show();
    } ;
    Fastbucks.spinner.stop = function( $element ){
        $(".fastbucks-spinner", $element).hide('slow').remove();
    };



})(AJS.$)