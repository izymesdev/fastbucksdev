package com.izymes.jira.fastbucks.clients.freshbooks;

import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.izymes.jira.fastbucks.api.FastbucksClient;
import com.izymes.jira.fastbucks.api.GenericInvoice;
import com.izymes.jira.fastbucks.api.GenericResponse;
import com.izymes.jira.fastbucks.clients.freshbooks.exceptions.ConnectionException;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Client;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Invoice;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Response;
import com.izymes.jira.fastbucks.clients.freshbooks.service.ClientService;
import com.izymes.jira.fastbucks.clients.freshbooks.service.FreshbooksServiceFactory;
import com.izymes.jira.fastbucks.clients.freshbooks.service.InvoiceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClientTest {
    @Mock
    private I18nResolver i18nResolver;
    @Mock private FreshbooksServiceFactory freshbooksServiceFactory;
    private Map<String,String> config;
    private FastbucksClient client;

    @Before
    public void setUp() throws Exception {
//        freshbooksServiceFactory = mock(FreshbooksServiceFactory.class);
        config = Maps.newHashMap();
        client = new FreshbooksClient( i18nResolver,freshbooksServiceFactory);
        config.put(FreshbooksClient.AUTH_TOKEN_KEY, "newKey");
        config.put(FreshbooksClient.URL_KEY, "newUrl");

        client.setConfig(config);
    }

    @Test
    public void testSetConfigMapNewToken(){

        Map<String, String > theConfig = client.getConfig();
        assertEquals( config.get(FreshbooksClient.AUTH_TOKEN_KEY), theConfig.get(FreshbooksClient.AUTH_TOKEN_KEY));
        assertEquals( config.get(FreshbooksClient.URL_KEY), theConfig.get(FreshbooksClient.URL_KEY));
    }
    @Test
    public void testSetConfigMapUpdateToken(){

        Map<String, String> newConfig = Maps.newHashMap();
        newConfig.put(FreshbooksClient.AUTH_TOKEN_KEY,"updatedKey");
        newConfig.put(FreshbooksClient.URL_KEY,"newUrl");

        client.setConfig( newConfig );

        Map<String, String > theConfig = client.getConfig();
        assertEquals( newConfig.get(FreshbooksClient.AUTH_TOKEN_KEY), theConfig.get(FreshbooksClient.AUTH_TOKEN_KEY));
        assertEquals( newConfig.get(FreshbooksClient.URL_KEY), theConfig.get(FreshbooksClient.URL_KEY));
    }
    @Test
    public void testSetConfigMapUpdateWithoutToken(){

        Map<String, String> newConfig = Maps.newHashMap();
        newConfig.put(FreshbooksClient.URL_KEY,"newUrl");

        client.setConfig( newConfig );

        Map<String, String > theConfig = client.getConfig();
        assertEquals( config.get(FreshbooksClient.AUTH_TOKEN_KEY), theConfig.get(FreshbooksClient.AUTH_TOKEN_KEY));
        assertEquals( newConfig.get(FreshbooksClient.URL_KEY), theConfig.get(FreshbooksClient.URL_KEY));
    }
    @Test
    public void testSetConfigMapUpdateEmptyToken(){

        Map<String, String> newConfig = Maps.newHashMap();
        newConfig.put(FreshbooksClient.URL_KEY,"newUrl");
        newConfig.put(FreshbooksClient.AUTH_TOKEN_KEY,"");

        client.setConfig( newConfig );

        Map<String, String > theConfig = client.getConfig();
        assertEquals( config.get(FreshbooksClient.AUTH_TOKEN_KEY), theConfig.get(FreshbooksClient.AUTH_TOKEN_KEY));
        assertEquals( newConfig.get(FreshbooksClient.URL_KEY), theConfig.get(FreshbooksClient.URL_KEY));
    }

    @Test
    public void testSendInvoiceWithLongNumber(){
        prepareInvoiceService();
        final List<String> genericInvoiceNumbers = Lists.newArrayList( "INV-TEST-2012-09-25-20-48",
                "INV-TEST-2003-09-25-20-48",
                "INV-TEST-2033-09-25-20-48");

        for (String genericInvoiceNumber : genericInvoiceNumbers) {
            GenericInvoice genericInvoice = new GenericInvoice();
            genericInvoice.setInvoiceNumber(genericInvoiceNumber);
            genericInvoice.setContactName("J R");

            GenericResponse genericResponse = client.sendInvoice(genericInvoice);
            assertTrue("returned with exception " + genericInvoiceNumber, genericResponse.getException() == null);
            assertTrue(genericResponse.getMessage().contains("sent"));
            String invoiceNumber = genericResponse.getConfirmedInvoice().getInvoiceNumber();
            Date dateOfInvoice = new Date((Long.parseLong(invoiceNumber) * 60000L) + FreshbooksClient.YEAR_OF_JIRA);
            String invDate = "INV-TEST-" + new SimpleDateFormat("yyyy-MM-dd-HH-mm").format(dateOfInvoice);
            assertEquals(invDate, genericInvoiceNumber);
        }
    }

    private void prepareInvoiceService() {
        when(freshbooksServiceFactory.getService(ClientService.class)).thenReturn(new ClientService(){
            public Client get(){
                Client client = new Client();
                client.setLastName("J");
                return client;
            }
        });
        when(freshbooksServiceFactory.getService(InvoiceService.class)).thenReturn(new InvoiceService(){
            public Response create(Invoice arg) throws ConnectionException {
                Response response = new Response();
                if ( arg.getNumber().length() > 10 ){
                    throw new ConnectionException("invoice number length > 10");
                }
                String id = arg.getNumber().split("TEST")[1];
                response.setInvoiceId( Long.decode("0x"+id) );
                return response;
            }
        });
        when(i18nResolver.getText(eq("fastbucks.invoice.sent"), anyLong() )).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return "sent " + invocation.getArguments()[0];
            }
        });
    }

}
