package com.izymes.jira.fastbucks.clients.freshbooks;

import com.izymes.jira.fastbucks.clients.freshbooks.model.System;
import com.thoughtworks.xstream.XStream;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class SerializationTest
{
    @Test
    public void testSystem()
    {
        System system = new System();
        system.setCurrencyCode("AUD");
        system.setCompanyName("funky dudes");
        System.Api api =  new System.Api();
        api.setRequestLimit(6000);
        api.setRequests(16);
        system.setApi( api );

        XStream xs = new CustomXStream();

        String xml = xs.toXML(system);

        System systemFromXml = (System)xs.fromXML( xml );

        assertEquals( system.getCompanyName(), systemFromXml.getCompanyName() );
        assertEquals( system.getApi().getRequests(),  systemFromXml.getApi().getRequests() );

    }
}
