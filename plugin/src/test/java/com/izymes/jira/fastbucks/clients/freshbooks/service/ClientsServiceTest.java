package com.izymes.jira.fastbucks.clients.freshbooks.service;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.izymes.jira.fastbucks.clients.freshbooks.FreshbooksConnection;
import com.izymes.jira.fastbucks.clients.freshbooks.exceptions.ConnectionException;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Client;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Clients;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Request;
import com.izymes.jira.fastbucks.clients.freshbooks.model.Response;
import org.apache.commons.collections.ListUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static junit.framework.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ClientsServiceTest {


    private Client c1;
    private Client c2;
    private Client c3;
    ClientsService service;

    @Before
    public void setUp(){
        c1 = new Client();
        c1.setLastName("client1");
        c2 = new Client();
        c2.setLastName("client2");
        c3 = new Client();
        c3.setLastName("client3");

        service = new ClientsService();
    }

    @Test
    public void testPagedResponse() throws ConnectionException {

        FreshbooksConnection connection = new FreshbooksConnection() {
            @Override
            public Response performRequest(Request request) throws ConnectionException {
                Response response = new Response();
                Clients clients = new Clients();
                switch (request.getPage()){
                    case 1:
                        clients.setContents( Lists.newArrayList(c1) );
                        clients.setPage(1);
                        clients.setPages(3);
                        break;
                    case 2:
                        clients.setContents( Lists.newArrayList(c2) );
                        clients.setPage(2);
                        clients.setPages(3);
                        break;
                    case 3:
                        clients.setContents(Lists.newArrayList(c3));
                        clients.setPage(3);
                        clients.setPages(3);
                }
                response.setClients( clients );
                return response;
            }
        };

        service.setConnection( connection );

        Clients allClients = service.get();
        assertEquals(3, allClients.getClients().size());
        final List<String> clientNames = Lists.newArrayList( c1.getLastName(), c2.getLastName(), c3.getLastName() );

        List<String> allClientNames = Lists.transform(allClients.getClients(), new Function<Client, String>() {
            @Override
            public String apply(Client from) {
                return from.getLastName();
            }
        });

        List result = ListUtils.intersection( allClientNames, clientNames);
        assertEquals(3, result.size());
    }
}
