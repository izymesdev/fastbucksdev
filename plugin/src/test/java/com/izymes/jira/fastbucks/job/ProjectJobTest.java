package com.izymes.jira.fastbucks.job;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.worklog.WorklogService;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.google.common.collect.Maps;
import com.izymes.jira.fastbucks.modules.*;
import mock.MockPluginScheduler;
import mock.MockPluginSettingsFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.Map;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ProjectJobTest {

    PluginSettingsFactory pluginSettingsFactory = new MockPluginSettingsFactory();
    ProjectConfigManager projectConfigManager;
    @Mock
    ClientManager clientManager;
    @Mock
    UserManager userManager;
    @Mock
    SearchService searchService;
    @Mock
    WorklogService worklogService;
    @Mock
    JqlQueryBuilder jqlQueryBuilder;
    PluginScheduler pluginScheduler;
    @Mock
    ProjectManager projectManager;
    @Mock
    MarketPlaceLicenseManager marketPlaceLicenseManager;
    @Mock
    EventPublisher eventPublisher;


    JobContainer jobContainer;
    ProjectJob job = new ProjectJob();
    private DefaultWorklogScheduler worklogScheduler;

    @Before
    public void setUp(){
        pluginScheduler = new MockPluginScheduler();
        projectConfigManager = new DefaultProjectConfigManager( pluginSettingsFactory, marketPlaceLicenseManager);
        jobContainer = new JobContainer( projectConfigManager, clientManager, userManager, searchService, worklogService, projectManager);
        worklogScheduler = new DefaultWorklogScheduler( projectConfigManager, jobContainer, pluginScheduler, marketPlaceLicenseManager, pluginSettingsFactory);
    }

    @Test
    public void testRemoveScheduledProject() throws InterruptedException {

        Map<String, Object> configA = Maps.newHashMap();
        configA.put( DefaultProjectConfigManager.projectConfigKeys.cronexpression.name(),  "0/2 * * * * ?" );
        projectConfigManager.setCommonProjectConfig("pa", configA);
        long now = new Date().getTime();
        worklogScheduler.setScheduler("pa");

        Thread.sleep(5000);
        verify(projectManager, times(1)).getProjectObjByKey("pa");
    }

}
