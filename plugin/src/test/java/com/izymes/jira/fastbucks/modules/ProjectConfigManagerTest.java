package com.izymes.jira.fastbucks.modules;


import com.atlassian.jira.web.component.cron.CronEditorBean;
import com.atlassian.jira.web.component.cron.CronEditorWebComponent;
import com.atlassian.jira.web.component.cron.parser.CronExpressionParser;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class ProjectConfigManagerTest {
    @Test
    public void testCronEditorHtml51(){


        String html = TestConstants.cronHtml51;
        Matcher m = Pattern.compile(DefaultProjectConfigManager.CRON_EDITOR_HTML_SCRIPT_PATTERN, Pattern.DOTALL).matcher(html);
        m.find();
        String plainHtml = m.replaceAll("");
        assertFalse(plainHtml.contains("<script"));
        assertTrue(plainHtml.contains("daysOfMonth"));

    }
    @Test
    public void testCronEditorHtml50(){


        String html = TestConstants.cronHtml50;
        Matcher m = Pattern.compile(DefaultProjectConfigManager.CRON_EDITOR_HTML_SCRIPT_PATTERN, Pattern.DOTALL).matcher(html);
        m.find();
        String plainHtml = m.replaceAll("");
        assertFalse(plainHtml.contains("<script"));
        assertTrue(plainHtml.contains("daysOfMonth"));

    }
}
