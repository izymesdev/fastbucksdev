package com.izymes.jira.fastbucks.modules;

public abstract class TestConstants {
    public static final String  cronHtml51="<fieldset class=\"group\">\n" +
            "    <legend><span>Schedule</span></legend>\n" +
            "    <div class=\"radio\">\n" +
            "        <input value=\"daily\" id=\"fastbucks.daily-option\" name=\"fastbucks.dailyWeeklyMonthly\" onclick=\"switchToDaily('fastbucks.');\" type=\"radio\"  />\n" +
            "        <label for=\"fastbucks.daily-option\">Daily</label>\n" +
            "    </div>\n" +
            "    <div class=\"radio\">\n" +
            "        <input value=\"daysOfWeek\" id=\"fastbucks.daysOfWeek-option\" name=\"fastbucks.dailyWeeklyMonthly\" onclick=\"switchToDaysOfWeek('fastbucks.');\" type=\"radio\"  />\n" +
            "        <label for=\"fastbucks.daysOfWeek-option\">Days per Week</label>\n" +
            "    </div>\n" +
            "    <div class=\"radio\">\n" +
            "        <input value=\"daysOfMonth\" id=\"fastbucks.daysOfMonth-option\" name=\"fastbucks.dailyWeeklyMonthly\" onclick=\"switchToDaysOfMonth('fastbucks.');\" type=\"radio\" checked  />\n" +
            "        <label for=\"fastbucks.daysOfMonth-option\">Days per Month</label>\n" +
            "    </div>\n" +
            "    <div class=\"radio\">\n" +
            "        <input value=\"advanced\" id=\"fastbucks.advanced-option\" name=\"fastbucks.dailyWeeklyMonthly\" onclick=\"switchToAdvanced('fastbucks.');\" type=\"radio\"  />\n" +
            "        <label for=\"fastbucks.advanced-option\">Advanced</label>\n" +
            "    </div>\n" +
            "</fieldset>\n" +
            "<fieldset class=\"group\">\n" +
            "    <legend><span><label for=\"fastbucks.interval\">Interval</label></span></legend>\n" +
            "    <div class=\"cron-options-tab-panel\" style=\"display: none;padding-bottom:8px;\" id=\"fastbucks.daysOfMonth\">\n" +
            "        <div class=\"radio\">\n" +
            "            <input value=\"dayOfMonth\" name=\"fastbucks.daysOfMonthOpt\" id=\"fastbucks.dayOfMonthRadio\" type=\"radio\"  checked  />\n" +
            "            <select id='fastbucks.monthDay' onchange=\"document.getElementById('fastbucks.dayOfMonthRadio').checked = true;\" name='fastbucks.monthDay'>\n" +
            "                <option value='1'>1st</option>\n" +
            "                <option value='2'>2nd</option>\n" +
            "                <option value='3'>3rd</option>\n" +
            "                <option value='4'>4th</option>\n" +
            "                <option value='5'>5th</option>\n" +
            "                <option value='6'>6th</option>\n" +
            "                <option value='7'>7th</option>\n" +
            "                <option value='8'>8th</option>\n" +
            "                <option value='9'>9th</option>\n" +
            "                <option value='10'>10th</option>\n" +
            "                <option value='11'>11th</option>\n" +
            "                <option value='12'>12th</option>\n" +
            "                <option value='13'>13th</option>\n" +
            "                <option value='14'>14th</option>\n" +
            "                <option value='15'>15th</option>\n" +
            "                <option value='16'>16th</option>\n" +
            "                <option value='17'>17th</option>\n" +
            "                <option value='18'>18th</option>\n" +
            "                <option value='19'>19th</option>\n" +
            "                <option value='20'>20th</option>\n" +
            "                <option value='21'>21st</option>\n" +
            "                <option value='22'>22nd</option>\n" +
            "                <option value='23'>23rd</option>\n" +
            "                <option value='24'>24th</option>\n" +
            "                <option value='25'>25th</option>\n" +
            "                <option value='26'>26th</option>\n" +
            "                <option value='27'>27th</option>\n" +
            "                <option value='28'>28th</option>\n" +
            "                <option value='29'>29th</option>\n" +
            "                <option value='30'>30th</option>\n" +
            "                <option value='31'>31st</option>\n" +
            "                <option value='L'>last</option>\n" +
            "            </select>\n" +
            "            <label>day of every month</label>\n" +
            "        </div>\n" +
            "        <div class=\"radio\">\n" +
            "            <input value=\"dayOfWeekOfMonth\" name=\"fastbucks.daysOfMonthOpt\" id=\"fastbucks.dayOfWeekOfMonthRadio\" type=\"radio\"  />\n" +
            "            <select name='fastbucks.week' id='fastbucks.week' onchange=\"document.getElementById('fastbucks.dayOfWeekOfMonthRadio').checked = true;\">\n" +
            "                <option value='1'>first</option>\n" +
            "                <option value='2'>second</option>\n" +
            "                <option value='3'>third</option>\n" +
            "                <option value='4'>fourth</option>\n" +
            "                <option value='L'>last</option>\n" +
            "            </select>\n" +
            "            <select name='fastbucks.day' id='fastbucks.day' onchange=\"document.getElementById('fastbucks.dayOfWeekOfMonthRadio').checked = true;\">\n" +
            "                <option value='1'>Sunday</option>\n" +
            "                <option value='2'>Monday</option>\n" +
            "                <option value='3'>Tuesday</option>\n" +
            "                <option value='4'>Wednesday</option>\n" +
            "                <option value='5'>Thursday</option>\n" +
            "                <option value='6'>Friday</option>\n" +
            "                <option value='7'>Saturday</option>\n" +
            "            </select>\n" +
            "            <label>of every month</label>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "    <div class=\"cron-options-tab-panel\" id=\"fastbucks.freqDiv\">\n" +
            "        <div class=\"field-group\">\n" +
            "            <span id=\"fastbucks.innerFreqDiv\">\n" +
            "                <select id=\"fastbucks.interval\" name=\"fastbucks.interval\" onChange=\"toggleFrequencyControl('fastbucks.', true);\">\n" +
            "                    <option selected  value=\"0\">once per day</option>\n" +
            "                    <option  value=\"180\">every 3 hours</option>\n" +
            "                    <option  value=\"120\">every 2 hours</option>\n" +
            "                    <option  value=\"60\">every hour</option>\n" +
            "                    <option  value=\"30\">every 30 minutes</option>\n" +
            "                    <option  value=\"15\">every 15 minutes</option>\n" +
            "                </select>\n" +
            "            </span>\n" +
            "            <span id=\"fastbucks.runOnce\">\n" +
            "                <label for=\"fastbucks.runOnceHours\">at</label>\n" +
            "                <select id=\"fastbucks.runOnceHours\" name=\"fastbucks.runOnceHours\">\n" +
            "                                            <option  value=\"1\">1</option>\n" +
            "                                            <option  value=\"2\">2</option>\n" +
            "                                            <option  value=\"3\">3</option>\n" +
            "                                            <option  value=\"4\">4</option>\n" +
            "                                            <option  value=\"5\">5</option>\n" +
            "                                            <option  value=\"6\">6</option>\n" +
            "                                            <option  value=\"7\">7</option>\n" +
            "                                            <option  value=\"8\">8</option>\n" +
            "                                            <option  value=\"9\">9</option>\n" +
            "                                            <option  value=\"10\">10</option>\n" +
            "                                            <option selected value=\"11\">11</option>\n" +
            "                                            <option  value=\"12\">12</option>\n" +
            "                                    </select>\n" +
            "                <select name=\"fastbucks.runOnceMins\">\n" +
            "                    <option selected  value=\"0\">00</option>\n" +
            "                    <option  value=\"5\">05</option>\n" +
            "                    <option  value=\"10\">10</option>\n" +
            "                    <option  value=\"15\">15</option>\n" +
            "                    <option  value=\"20\">20</option>\n" +
            "                    <option  value=\"25\">25</option>\n" +
            "                    <option  value=\"30\">30</option>\n" +
            "                    <option  value=\"35\">35</option>\n" +
            "                    <option  value=\"40\">40</option>\n" +
            "                    <option  value=\"45\">45</option>\n" +
            "                    <option  value=\"50\">50</option>\n" +
            "                    <option  value=\"55\">55</option>\n" +
            "                </select>\n" +
            "                <select name=\"fastbucks.runOnceMeridian\">\n" +
            "                    <option  value=\"am\">am</option>\n" +
            "                    <option selected  value=\"pm\">pm</option>\n" +
            "                </select>\n" +
            "            </span>\n" +
            "            <span style=\"display: none;\" id=\"fastbucks.runMany\">\n" +
            "                <label for=\"fastbucks.runFromHours\">from</label>\n" +
            "                <select id=\"fastbucks.runFromHours\" name=\"fastbucks.runFromHours\">\n" +
            "                                            <option  value=\"1\">1</option>\n" +
            "                                            <option  value=\"2\">2</option>\n" +
            "                                            <option  value=\"3\">3</option>\n" +
            "                                            <option  value=\"4\">4</option>\n" +
            "                                            <option  value=\"5\">5</option>\n" +
            "                                            <option  value=\"6\">6</option>\n" +
            "                                            <option  value=\"7\">7</option>\n" +
            "                                            <option  value=\"8\">8</option>\n" +
            "                                            <option  value=\"9\">9</option>\n" +
            "                                            <option  value=\"10\">10</option>\n" +
            "                                            <option  value=\"11\">11</option>\n" +
            "                                            <option  value=\"12\">12</option>\n" +
            "                                    </select>\n" +
            "                <select name=\"fastbucks.runFromMeridian\">\n" +
            "                    <option  value=\"am\">am</option>\n" +
            "                    <option  value=\"pm\">pm</option>\n" +
            "                </select>\n" +
            "                <label for=\"fastbucks.runToHours\">to</label>\n" +
            "                <select id=\"fastbucks.runToHours\" name=\"fastbucks.runToHours\">\n" +
            "                                            <option  value=\"1\">1</option>\n" +
            "                                            <option  value=\"2\">2</option>\n" +
            "                                            <option  value=\"3\">3</option>\n" +
            "                                            <option  value=\"4\">4</option>\n" +
            "                                            <option  value=\"5\">5</option>\n" +
            "                                            <option  value=\"6\">6</option>\n" +
            "                                            <option  value=\"7\">7</option>\n" +
            "                                            <option  value=\"8\">8</option>\n" +
            "                                            <option  value=\"9\">9</option>\n" +
            "                                            <option  value=\"10\">10</option>\n" +
            "                                            <option  value=\"11\">11</option>\n" +
            "                                            <option  value=\"12\">12</option>\n" +
            "                                    </select>\n" +
            "                <select name=\"fastbucks.runToMeridian\">\n" +
            "                    <option  value=\"am\">am</option>\n" +
            "                    <option  value=\"pm\">pm</option>\n" +
            "                </select>\n" +
            "            </span>\n" +
            "                    </div>\n" +
            "        <div class=\"description\">The current server time is 22/Aug/12 9:14 AM - Eastern Standard Time (Queensland)</div>\n" +
            "    </div>\n" +
            "    <div class=\"cron-options-tab-panel\" style=\"display: none;padding-top:8px;\" id=\"fastbucks.daysOfWeek\">\n" +
            "        <div class=\"checkbox\">\n" +
            "            <input id=\"fastbucks.monday\" name=\"fastbucks.weekday\" value=\"2\" type=\"checkbox\"  />\n" +
            "            <label>Monday</label>\n" +
            "        </div>\n" +
            "        <div class=\"checkbox\">\n" +
            "            <input id=\"fastbucks.tuesday\" name=\"fastbucks.weekday\" value=\"3\" type=\"checkbox\"  />\n" +
            "            <label for=\"fastbucks.tuesday\">Tuesday</label>\n" +
            "        </div>\n" +
            "        <div class=\"checkbox\">\n" +
            "            <input id=\"fastbucks.wednesday\" name=\"fastbucks.weekday\" value=\"4\" type=\"checkbox\"  />\n" +
            "            <label for=\"fastbucks.wednesday\">Wednesday</label>\n" +
            "        </div>\n" +
            "        <div class=\"checkbox\">\n" +
            "            <input id=\"fastbucks.thursday\" name=\"fastbucks.weekday\" value=\"5\" type=\"checkbox\"  />\n" +
            "            <label for=\"fastbucks.thursday\">Thursday</label>\n" +
            "        </div>\n" +
            "        <div class=\"checkbox\">\n" +
            "            <input id=\"fastbucks.friday\" name=\"fastbucks.weekday\" value=\"6\" type=\"checkbox\"  />\n" +
            "            <label for=\"fastbucks.friday\">Friday</label>\n" +
            "        </div>\n" +
            "        <div class=\"checkbox\">\n" +
            "            <input id=\"fastbucks.saturday\" name=\"fastbucks.weekday\" value=\"7\" type=\"checkbox\"  />\n" +
            "            <label for=\"fastbucks.saturday\">Saturday</label>\n" +
            "        </div>\n" +
            "        <div class=\"checkbox\">\n" +
            "            <input id=\"fastbucks.sunday\" name=\"fastbucks.weekday\" value=\"1\" type=\"checkbox\"  />\n" +
            "            <label for=\"fastbucks.sunday\">Sunday</label>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "    <div class=\"field-group cron-options-tab-panel\" style=\"display: none;\" id=\"fastbucks.advanced\">\n" +
            "        <input type=\"text\" size=\"30\" id=\"fastbucks.cronString\" name=\"fastbucks.cronString\" />\n" +
            "        \n" +
            "                    <a class=\"helpLink\" href=\"https://docs.atlassian.com/jira/docs-051/Receiving+Search+Results+via+Email\" target=\"_jirahelp\">\n" +
            "    \n" +
            "        <img src=\"/jira/images/icons/ico_help.png\" width=\"16\" height=\"16\" align=\"absmiddle\"\n" +
            "            title=\"Get online help about Receiving Filter Results via Email\"\n" +
            "        /></a>\n" +
            "        <div class=\"description\">Cron Expression</div>\n" +
            "    </div>\n" +
            "    <div class=\"error\"></div>\n" +
            "</fieldset>\n" +
            "\n" +
            "<script>\n" +
            "\n" +
            "    if (! timesOnce)\n" +
            "    {\n" +
            "        var timesOnce = new Object();\n" +
            "    }\n" +
            "    timesOnce[\"fastbucks.\"] = true ;\n" +
            "\n" +
            "        day = \"L\";\n" +
            "    if (day != \"*\" && day != \"?\")\n" +
            "    {\n" +
            "                if (day == \"L\")\n" +
            "        {\n" +
            "            day = 32;         }\n" +
            "        document.getElementById(\"fastbucks.monthDay\").selectedIndex=day -1;\n" +
            "    }\n" +
            "\n" +
            "                    weekDay = \"?\";\n" +
            "        document.getElementById(\"fastbucks.day\").selectedIndex = weekDay - 1;\n" +
            "                timesPerMonth = \"\";\n" +
            "        if (timesPerMonth)\n" +
            "        {\n" +
            "                        if (timesPerMonth == \"L\")\n" +
            "            {\n" +
            "                timesPerMonth = 5;             }\n" +
            "            document.getElementById(\"fastbucks.week\").selectedIndex = timesPerMonth - 1;\n" +
            "        }\n" +
            "    \n" +
            "            switchToDaysOfMonth('fastbucks.');\n" +
            "    \n" +
            "            switchToOnce('fastbucks.', false);\n" +
            "    </script>";
    public static final String  cronHtml50="    <label><input value=\"daily\" name=\"fastbucks.dailyWeeklyMonthly\" onclick=\"switchToDaily('fastbucks.');\" type=\"radio\" >\n" +
            "    Daily</label>\n" +
            "    <label><input value=\"daysOfWeek\" name=\"fastbucks.dailyWeeklyMonthly\" onclick=\"switchToDaysOfWeek('fastbucks.');\" type=\"radio\" >\n" +
            "    Days per Week</label>\n" +
            "    <label><input value=\"daysOfMonth\" name=\"fastbucks.dailyWeeklyMonthly\" onclick=\"switchToDaysOfMonth('fastbucks.');\" type=\"radio\" checked >\n" +
            "    Days per Month</label>\n" +
            "    <label><input value=\"advanced\" name=\"fastbucks.dailyWeeklyMonthly\" onclick=\"switchToAdvanced('fastbucks.');\" type=\"radio\" >\n" +
            "    Advanced</label>\n" +
            "    <br/>\n" +
            "    <div style=\"display: none;\" id=\"fastbucks.daysOfWeek\">\n" +
            "        <table>\n" +
            "        <tbody>\n" +
            "            <tr>\n" +
            "                <td><label><input name=\"fastbucks.weekday\" value=\"2\" type=\"checkbox\" >Monday</label><br/></td>\n" +
            "                <td><label><input name=\"fastbucks.weekday\" value=\"7\" type=\"checkbox\" >Saturday</label><br/></td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td><label><input name=\"fastbucks.weekday\" value=\"3\" type=\"checkbox\" >Tuesday</label><br/></td>\n" +
            "                <td><label><input name=\"fastbucks.weekday\" value=\"1\" type=\"checkbox\" >Sunday</label><br/></td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td><label><input name=\"fastbucks.weekday\" value=\"4\" type=\"checkbox\" >Wednesday</label><br/></td>\n" +
            "                <td></td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td><label><input name=\"fastbucks.weekday\" value=\"5\" type=\"checkbox\" >Thursday</label><br/></td>\n" +
            "                <td></td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td><label><input name=\"fastbucks.weekday\" value=\"6\" type=\"checkbox\" >Friday</label><br/></td>\n" +
            "                <td></td>\n" +
            "            </tr>\n" +
            "        </tbody></table>\n" +
            "    </div>\n" +
            "    <div style=\"display: none;\" id=\"fastbucks.daysOfMonth\">\n" +
            "                <table>\n" +
            "            <tbody>\n" +
            "            <tr>\n" +
            "                <td>\n" +
            "                    <input value=\"dayOfMonth\" name=\"fastbucks.daysOfMonthOpt\" id=\"fastbucks.dayOfMonthRadio\" type=\"radio\"  checked >\n" +
            "                </td>\n" +
            "                <td>\n" +
            "                                        The <select id='fastbucks.monthDay' onchange=\"getElementById('fastbucks.dayOfMonthRadio').checked = true;\" name='fastbucks.monthDay'> <option value='1'>1st</option> <option value='2'>2nd</option> <option value='3'>3rd</option> <option value='4'>4th</option> <option value='5'>5th</option> <option value='6'>6th</option> <option value='7'>7th</option> <option value='8'>8th</option> <option value='9'>9th</option> <option value='10'>10th</option> <option value='11'>11th</option> <option value='12'>12th</option> <option value='13'>13th</option> <option value='14'>14th</option> <option value='15'>15th</option> <option value='16'>16th</option> <option value='17'>17th</option> <option value='18'>18th</option> <option value='19'>19th</option> <option value='20'>20th</option> <option value='21'>21st</option> <option value='22'>22nd</option> <option value='23'>23rd</option> <option value='24'>24th</option> <option value='25'>25th</option> <option value='26'>26th</option> <option value='27'>27th</option> <option value='28'>28th</option> <option value='29'>29th</option> <option value='30'>30th</option> <option value='31'>31st</option> <option value='L'>last</option> </select> day of every month\n" +
            "                </td>\n" +
            "            </tr>\n" +
            "            <tr>\n" +
            "                <td>\n" +
            "                    <input value=\"dayOfWeekOfMonth\" name=\"fastbucks.daysOfMonthOpt\" id=\"fastbucks.dayOfWeekOfMonthRadio\" type=\"radio\" >\n" +
            "                </td>\n" +
            "                <td>\n" +
            "                                        The  <select name='fastbucks.week' id='fastbucks.week' onchange=\"getElementById('fastbucks.dayOfWeekOfMonthRadio').checked = true;\"> <option value='1'>first</option> <option value='2'>second</option> <option value='3'>third</option> <option value='4'>fourth</option> <option value='L'>last</option> </select> <select name='fastbucks.day' id='fastbucks.day' onchange=\"getElementById('fastbucks.dayOfWeekOfMonthRadio').checked = true;\"> <option value='1'>Sunday</option> <option value='2'>Monday</option> <option value='3'>Tuesday</option> <option value='4'>Wednesday</option> <option value='5'>Thursday</option> <option value='6'>Friday</option> <option value='7'>Saturday</option> </select> of every month.\n" +
            "                </td>\n" +
            "            </tr>\n" +
            "        </tbody></table>\n" +
            "    </div>\n" +
            "    <div style=\"\" id=\"fastbucks.freqDiv\">\n" +
            "        <div style=\"\" id=\"fastbucks.innerFreqDiv\">\n" +
            "            <table>\n" +
            "                <tbody><tr>\n" +
            "                    <td>\n" +
            "            Interval:\n" +
            "            <select id=\"fastbucks.interval\" name=\"fastbucks.interval\" onChange=\"toggleFrequencyControl('fastbucks.', true);\">\n" +
            "                <option selected label=\"once\" value=\"0\">once per day</option>\n" +
            "                <option  label=\"3hours\" value=\"180\">every 3 hours</option>\n" +
            "                <option  label=\"2hours\" value=\"120\">every 2 hours</option>\n" +
            "                <option  label=\"1hour\" value=\"60\">every hour</option>\n" +
            "                <option  label=\"30min\" value=\"30\">every 30 minutes</option>\n" +
            "                <option  label=\"15min\" value=\"15\">every 15 minutes</option>\n" +
            "            </select>\n" +
            "                    </td>\n" +
            "                </tr>\n" +
            "            </tbody></table>\n" +
            "        </div>\n" +
            "        <div id=\"fastbucks.runOnce\">\n" +
            "            <table>\n" +
            "                <tbody><tr>\n" +
            "                    <td>at:</td>\n" +
            "                    <td>\n" +
            "                        <select name=\"fastbucks.runOnceHours\">\n" +
            "                                                            <option  value=\"1\">1</option>\n" +
            "                                                            <option  value=\"2\">2</option>\n" +
            "                                                            <option  value=\"3\">3</option>\n" +
            "                                                            <option  value=\"4\">4</option>\n" +
            "                                                            <option  value=\"5\">5</option>\n" +
            "                                                            <option  value=\"6\">6</option>\n" +
            "                                                            <option  value=\"7\">7</option>\n" +
            "                                                            <option  value=\"8\">8</option>\n" +
            "                                                            <option  value=\"9\">9</option>\n" +
            "                                                            <option  value=\"10\">10</option>\n" +
            "                                                            <option selected  value=\"11\">11</option>\n" +
            "                                                            <option  value=\"12\">12</option>\n" +
            "                                                    </select>\n" +
            "                    </td>\n" +
            "                    <td class=\"noPaddingCron\">:</td>\n" +
            "                    <td>\n" +
            "                        <select name=\"fastbucks.runOnceMins\">\n" +
            "                            <option selected  value=\"0\">00</option>\n" +
            "                            <option  value=\"5\">05</option>\n" +
            "                            <option  value=\"10\">10</option>\n" +
            "                            <option  value=\"15\">15</option>\n" +
            "                            <option  value=\"20\">20</option>\n" +
            "                            <option  value=\"25\">25</option>\n" +
            "                            <option  value=\"30\">30</option>\n" +
            "                            <option  value=\"35\">35</option>\n" +
            "                            <option  value=\"40\">40</option>\n" +
            "                            <option  value=\"45\">45</option>\n" +
            "                            <option  value=\"50\">50</option>\n" +
            "                            <option  value=\"55\">55</option>\n" +
            "                        </select>\n" +
            "                    </td>\n" +
            "                    <td>\n" +
            "                        <select name=\"fastbucks.runOnceMeridian\">\n" +
            "                            <option  label=\"am\" value=\"am\">am</option>\n" +
            "                            <option selected  label=\"pm\" value=\"pm\">pm</option>\n" +
            "                        </select>\n" +
            "                    </td>\n" +
            "                </tr>\n" +
            "            </tbody></table>\n" +
            "        </div>\n" +
            "        <div style=\"display: none;\" id=\"fastbucks.runMany\">\n" +
            "            <table>\n" +
            "                <tbody><tr>\n" +
            "                    <td>from:</td>\n" +
            "                    <td>\n" +
            "                        <select name=\"fastbucks.runFromHours\">\n" +
            "                                                            <option  value=\"1\">1</option>\n" +
            "                                                            <option  value=\"2\">2</option>\n" +
            "                                                            <option  value=\"3\">3</option>\n" +
            "                                                            <option  value=\"4\">4</option>\n" +
            "                                                            <option  value=\"5\">5</option>\n" +
            "                                                            <option  value=\"6\">6</option>\n" +
            "                                                            <option  value=\"7\">7</option>\n" +
            "                                                            <option  value=\"8\">8</option>\n" +
            "                                                            <option  value=\"9\">9</option>\n" +
            "                                                            <option  value=\"10\">10</option>\n" +
            "                                                            <option  value=\"11\">11</option>\n" +
            "                                                            <option  value=\"12\">12</option>\n" +
            "                                                    </select>\n" +
            "                    </td>\n" +
            "                    <td class=\"noPaddingCron\">:</td>\n" +
            "                    <td><input name=\"fastbucks.runFromMins\" size=\"2\" maxlength=\"2\" value=\"00\" disabled=\"disabled\"></td>\n" +
            "                    <td>\n" +
            "                        <select name=\"fastbucks.runFromMeridian\">\n" +
            "                            <option  label=\"am\" value=\"am\">am</option>\n" +
            "                            <option  label=\"pm\" value=\"pm\">pm</option>\n" +
            "                        </select>\n" +
            "                    </td>\n" +
            "                </tr>\n" +
            "                <tr>\n" +
            "                    <td>to:</td>\n" +
            "                    <td>\n" +
            "                        <select name=\"fastbucks.runToHours\">\n" +
            "                                                            <option  value=\"1\">1</option>\n" +
            "                                                            <option  value=\"2\">2</option>\n" +
            "                                                            <option  value=\"3\">3</option>\n" +
            "                                                            <option  value=\"4\">4</option>\n" +
            "                                                            <option  value=\"5\">5</option>\n" +
            "                                                            <option  value=\"6\">6</option>\n" +
            "                                                            <option  value=\"7\">7</option>\n" +
            "                                                            <option  value=\"8\">8</option>\n" +
            "                                                            <option  value=\"9\">9</option>\n" +
            "                                                            <option  value=\"10\">10</option>\n" +
            "                                                            <option  value=\"11\">11</option>\n" +
            "                                                            <option  value=\"12\">12</option>\n" +
            "                                                    </select>\n" +
            "                    </td>\n" +
            "                    <td class=\"noPaddingCron\">:</td>\n" +
            "                    <td><input name=\"fastbucks.runToMins\" size=\"2\" maxlength=\"2\" value=\"00\" disabled=\"disabled\"></td>\n" +
            "                    <td>\n" +
            "                        <select name=\"fastbucks.runToMeridian\">\n" +
            "                            <option  label=\"am\" value=\"am\">am</option>\n" +
            "                            <option  label=\"pm\" value=\"pm\">pm</option>\n" +
            "                        </select>\n" +
            "                    </td>\n" +
            "                </tr>\n" +
            "            </tbody></table>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "    <div style=\"display: none;\" id=\"fastbucks.advanced\">\n" +
            "        <table>\n" +
            "            <tbody><tr>\n" +
            "                <td>Cron Expression:</td>\n" +
            "                <td>\n" +
            "                    <input type=\"text\" size=\"30\" id=\"fastbucks.cronString\" name=\"fastbucks.cronString\" /> \n" +
            "                    <a class=\"helpLink\" href=\"https://docs.atlassian.com/jira/docs-050/Receiving+Search+Results+via+Email\" target=\"_jirahelp\">\n" +
            "    \n" +
            "        <img src=\"/jira/images/icons/ico_help.png\" width=\"16\" height=\"16\" align=\"absmiddle\"\n" +
            "            title=\"Get online help about Receiving Filter Results via Email\"\n" +
            "        /></a>\n" +
            "                </td>\n" +
            "                </tr>\n" +
            "            </tbody>\n" +
            "        </table>\n" +
            "    </div>\n" +
            "    <span class=\"red-highlight-small\">Note:</span><span class=\"small\">The current server time is 23/Aug/12 8:46 PM - Eastern Standard Time (Queensland)</span>    \n" +
            "    <script type=\"text/javascript\" language=\"JavaScript\">\n" +
            "\n" +
            "        if (! timesOnce)\n" +
            "        {\n" +
            "            var timesOnce = new Object();\n" +
            "        }\n" +
            "        timesOnce[\"fastbucks.\"] = true ;\n" +
            "\n" +
            "                day = \"L\";\n" +
            "        if (day != \"*\" && day != \"?\")\n" +
            "        {\n" +
            "                        if (day == \"L\")\n" +
            "            {\n" +
            "                day = 32;             }\n" +
            "            document.getElementById(\"fastbucks.monthDay\").selectedIndex=day -1;\n" +
            "        }\n" +
            "\n" +
            "                                weekDay = \"?\";\n" +
            "            document.getElementById(\"fastbucks.day\").selectedIndex = weekDay - 1;\n" +
            "                        timesPerMonth = \"\";\n" +
            "            if (timesPerMonth)\n" +
            "            {\n" +
            "                                if (timesPerMonth == \"L\")\n" +
            "                {\n" +
            "                    timesPerMonth = 5;                 }\n" +
            "                document.getElementById(\"fastbucks.week\").selectedIndex = timesPerMonth - 1;\n" +
            "            }\n" +
            "        \n" +
            "                    switchToDaysOfMonth('fastbucks.');\n" +
            "        \n" +
            "                    switchToOnce('fastbucks.', false);\n" +
            "            </script>";
}
