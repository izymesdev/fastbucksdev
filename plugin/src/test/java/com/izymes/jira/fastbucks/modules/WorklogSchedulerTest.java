package com.izymes.jira.fastbucks.modules;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.worklog.WorklogService;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.google.common.collect.Maps;
import com.izymes.jira.fastbucks.job.ProjectJob;
import mock.MockPluginScheduler;
import mock.MockPluginSettingsFactory;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.quartz.CronTrigger;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JqlQueryBuilder.class)
public class WorklogSchedulerTest {

    PluginSettingsFactory pluginSettingsFactory = new MockPluginSettingsFactory();
    ProjectConfigManager projectConfigManager;
    @Mock    ClientManager clientManager;
    @Mock    UserManager userManager;
    @Mock    SearchService searchService;
    @Mock    WorklogService worklogService;
    @Mock    JqlQueryBuilder jqlQueryBuilder;
    PluginScheduler pluginScheduler;
    @Mock    ProjectManager projectManager;
    @Mock    MarketPlaceLicenseManager marketPlaceLicenseManager;
    @Mock    EventPublisher  eventPublisher;


    JobContainer jobContainer;
    private DefaultWorklogScheduler worklogScheduler;


    @Ignore
    @Before
    public void setUp() throws Exception {
        clientManager = mock( ClientManager.class );
        userManager = mock( UserManager.class );
        searchService = mock( SearchService.class );
        jqlQueryBuilder = mock( JqlQueryBuilder.class );
        marketPlaceLicenseManager = mock( MarketPlaceLicenseManager.class );
        eventPublisher = mock( EventPublisher.class );
        pluginScheduler = new MockPluginScheduler();

        projectConfigManager = new DefaultProjectConfigManager( pluginSettingsFactory, marketPlaceLicenseManager);
        jobContainer = new JobContainer( projectConfigManager, clientManager, userManager, searchService, worklogService, projectManager);
        worklogScheduler = new DefaultWorklogScheduler( projectConfigManager, jobContainer, pluginScheduler, marketPlaceLicenseManager, pluginSettingsFactory);
    }

    @Test
    public void testScheduleMultipleProjects() throws InterruptedException {

        worklogScheduler = new DefaultWorklogScheduler( projectConfigManager, jobContainer, pluginScheduler, marketPlaceLicenseManager, pluginSettingsFactory);

        Map<String, Object> configA = Maps.newHashMap();
        configA.put( DefaultProjectConfigManager.projectConfigKeys.cronexpression.name(),  "0 0/1 * * * ?" );
        Map<String, Object> configB = Maps.newHashMap();
        configB.put(DefaultProjectConfigManager.projectConfigKeys.cronexpression.name(), "0 0/2 * * * ?");

        projectConfigManager.setCommonProjectConfig("pa", configA);
        projectConfigManager.setCommonProjectConfig("pb", configB);

        Date now = new Date();
        worklogScheduler.updateScheduler("pa", now);
        worklogScheduler.updateScheduler("pb", now);

        assertLastRunPersisted("pa", now);
        assertLastRunPersisted("pb", now);

        String jobKey = ProjectJob.KEY + "pa";
        assertSchedulerParams(now, jobKey, 60000L);
        jobKey = ProjectJob.KEY + "pb";
        assertSchedulerParams(now, jobKey, 120000L);
    }

    @Test
    public void testSetSchedulerLastFridayEndOfMonthAndRescheduleNextEndOfMonth() throws ParseException, InterruptedException {
        worklogScheduler = new DefaultWorklogScheduler( projectConfigManager, jobContainer, pluginScheduler, marketPlaceLicenseManager, pluginSettingsFactory);
        Map<String, Object> configA = Maps.newHashMap();
        String cronExpression = "0 0 23 ? * 6L";
        configA.put( DefaultProjectConfigManager.projectConfigKeys.cronexpression.name(), cronExpression); //the last friday of every month
        projectConfigManager.setCommonProjectConfig("pa", configA);
        Date now = new Date();
        worklogScheduler.setScheduler("pa");
        MockPluginScheduler mockPluginScheduler = (MockPluginScheduler) pluginScheduler;
        String jobKey = ProjectJob.KEY + "pa";
        assertTrue( mockPluginScheduler.getScheduledJobStartTime( jobKey ).getTime() % 60*60000L == 0 );
        assertEquals(5, mockPluginScheduler.getScheduledJobStartTime(jobKey).getDay()); // friday == 5
        assertEquals( now.getMonth(), mockPluginScheduler.getScheduledJobStartTime( jobKey ).getMonth() ); // this month   , August = 7

        CronTrigger cron = new CronTrigger();
        cron.setCronExpression(cronExpression);
        cron.triggered(null);
        assertEquals( cron.getNextFireTime(), mockPluginScheduler.getScheduledJobStartTime( jobKey ) );

        worklogScheduler.updateScheduler("pa", new Date(cron.getNextFireTime().getTime() + 5000L) );
        assertTrue( mockPluginScheduler.getScheduledJobStartTime( jobKey ).getTime() % 60*60000L == 0 );
        assertEquals(5, mockPluginScheduler.getScheduledJobStartTime(jobKey).getDay()); // friday == 5
    }

    private void assertSchedulerParams(Date now, String jobKey, long startTimeModuloMS ) {
        MockPluginScheduler mockPluginScheduler = (MockPluginScheduler) pluginScheduler;
        assertEquals( mockPluginScheduler.getScheduledJobDataMap(jobKey).get("lastrun"), now);
        assertTrue(mockPluginScheduler.getScheduledJobStartTime(jobKey).after(now));
        assertTrue( mockPluginScheduler.getScheduledJobStartTime( jobKey ).getTime() % startTimeModuloMS == 0 );
    }

    private void assertLastRunPersisted(String projectKey, Date timeStamp) {
        PluginSettings pluginSettings = pluginSettingsFactory.createSettingsForKey(projectKey);
        long lastRun = Long.parseLong( (String)pluginSettings.get( DefaultWorklogScheduler.PROJECT_SCHEDULER_LAST_RUN_KEY ));
        assertEquals( timeStamp.getTime(), lastRun );
    }

    @Test
    public void testSetConfigurationWhenAlreadyRunning(){
        Map<String, Object> configA = Maps.newHashMap();
        configA.put( DefaultProjectConfigManager.projectConfigKeys.cronexpression.name(),  "0 0 0/1 * * ?" );
        projectConfigManager.setCommonProjectConfig("pa", configA);

        PluginSettings pluginSettings = pluginSettingsFactory.createSettingsForKey("pa");
        long timeLastRun = new Date().getTime() - 50000 ;
        pluginSettings.put( DefaultWorklogScheduler.PROJECT_SCHEDULER_LAST_RUN_KEY, timeLastRun+"" );

        worklogScheduler.setScheduler( "pa" );
        assertEquals( timeLastRun+"", pluginSettings.get( DefaultWorklogScheduler.PROJECT_SCHEDULER_LAST_RUN_KEY+"" ));
    }
    @Test
    public void testSetConfigurationWhenNew(){
        Map<String, Object> configA = Maps.newHashMap();
        configA.put( DefaultProjectConfigManager.projectConfigKeys.cronexpression.name(),  "0 0 0/1 * * ?" );
        projectConfigManager.setCommonProjectConfig("pa", configA);
        long now = new Date().getTime();
        worklogScheduler.setScheduler("pa");
        PluginSettings pluginSettings = pluginSettingsFactory.createSettingsForKey("pa");
        long timeLastRun = Long.parseLong( (String)pluginSettings.get( DefaultWorklogScheduler.PROJECT_SCHEDULER_LAST_RUN_KEY ) );
        assertTrue( timeLastRun > now - 50);

    }
}
