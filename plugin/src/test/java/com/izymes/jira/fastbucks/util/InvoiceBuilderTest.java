package com.izymes.jira.fastbucks.util;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.issue.worklog.WorklogInputParameters;
import com.atlassian.jira.bc.issue.worklog.WorklogResult;
import com.atlassian.jira.bc.issue.worklog.WorklogService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogImpl;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.mail.DummyUser;
import com.google.common.collect.Lists;
import com.izymes.jira.fastbucks.api.GenericInvoice;
import com.izymes.jira.fastbucks.api.GenericItem;
import com.izymes.jira.fastbucks.model.CommonProjectConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceBuilderTest {

    @Mock
    private Issue issue;
    @Mock
    WorklogService worklogService;
    @Mock private WorklogManager worklogManager;
    private JiraServiceContext serviceContext;
    private InvoiceBuilder invoiceBuilder;
    private CommonProjectConfig projectConfig;
    private final static String PROJECT_KEY = "KA";

    @Before
    public void setUp() {
        invoiceBuilder = InvoiceBuilder.INSTANCE.create();
        User user = new DummyUser("fred");
        serviceContext = new JiraServiceContextImpl(user);
        invoiceBuilder.setJiraServiceContext(serviceContext).setWorklogService( worklogService );
        projectConfig = new CommonProjectConfig();
        projectConfig.setProjectRate(60.00f);
        invoiceBuilder.setProjectConfig(PROJECT_KEY, projectConfig);
        when(issue.getKey()).thenReturn("KA-1");
        when(issue.getSummary()).thenReturn("Issue Summary");
        when(issue.getDescription()).thenReturn("Issue Description");
    }

    @Test
    public void testTagIssueWorklogXero(){
        Worklog worklog = new WorklogImpl(worklogManager, issue, 123l, "fred", "comment", new Date(), "", null, 72000L);
        when(worklogService.getByIssue(serviceContext, issue)).thenReturn(Lists.newArrayList(worklog));
        invoiceBuilder.tagIssueWorklog("INV-123-456", issue);
        when(worklogService.validateUpdate(eq(serviceContext), any(WorklogInputParameters.class))).thenAnswer(new WorklogResultAnswer());
        verify(worklogService).validateUpdate(eq(serviceContext), any(WorklogInputParameters.class));

    }
    @Test
    public void testTagIssueWorklogFreshbooks(){
        Worklog worklog = new WorklogImpl(worklogManager, issue, 123l, "fred", "comment", new Date(), "", null, 72000L);
        when(worklogService.getByIssue(serviceContext, issue)).thenReturn(Lists.newArrayList(worklog));
        invoiceBuilder.tagIssueWorklog("123456", issue );
        when(worklogService.validateUpdate(eq(serviceContext), any(WorklogInputParameters.class))).thenAnswer(new WorklogResultAnswer());
        verify(worklogService).validateUpdate(eq(serviceContext), any(WorklogInputParameters.class));
    }
    @Test
    public void testDontTagAlreadyTaggedIssueWorklog(){
        Worklog worklog = new WorklogImpl(worklogManager, issue, 123l, "fred", "comment \n T-INV-KA-2012-12-03-09-00", new Date(), "", null, 72000L);
        when(worklogService.getByIssue(serviceContext, issue)).thenReturn(Lists.newArrayList(worklog));
        invoiceBuilder.tagIssueWorklog("INV-123-456", issue);
        verify(worklogService,never()).validateUpdate(eq(serviceContext), any(WorklogInputParameters.class));
    }


    @Test
    public void testAddIssueWorklog() throws InterruptedException {
        List<Worklog> worklog = Lists.newArrayList();
        Date startDate = new Date();
        worklog.add(new WorklogImpl(worklogManager, issue, 123L, "fred", "work", startDate, "", null, 72000L));
        worklog.add( new WorklogImpl(worklogManager, issue, 124L, "fred", "more", new Date(startDate.getTime()+100), "", null, 36000L));

        when(worklogService.getByIssue(serviceContext, issue)).thenReturn(worklog);
        when(issue.getUpdated()).thenReturn( new Timestamp( startDate.getTime() + 100 ));
        invoiceBuilder.addIssueWorklog(issue, 0);

        GenericInvoice invoice = invoiceBuilder.build();
        assertEquals(1, invoice.getItems().size() );
        assertEquals(((72000L+36000L)/3600)+".0",  invoice.getItems().get(0).getQuantity()+"" );
    }
    @Test
    public void testAddIssueWorklogTaggedItemXero() throws InterruptedException {
        List<Worklog> worklog = Lists.newArrayList();
        Date startDate = new Date();
        worklog.add(new WorklogImpl(worklogManager, issue, 123L, "fred", "work \n T-INV-KA", startDate, "", null, 72000L));
        worklog.add( new WorklogImpl(worklogManager, issue, 124L, "fred", "more", new Date(startDate.getTime()+100), "", null, 36000L));

        when(worklogService.getByIssue(serviceContext, issue)).thenReturn(worklog);
        when(issue.getUpdated()).thenReturn( new Timestamp( startDate.getTime() + 100 ));
        invoiceBuilder.addIssueWorklog(issue, 0);

        GenericInvoice invoice = invoiceBuilder.build();
        assertEquals(1, invoice.getItems().size() );
        assertEquals(((36000L)/3600)+".0",  invoice.getItems().get(0).getQuantity()+"" );
        assertTrue(invoice.getInvoiceNumber().contains("INV-KA-"));
    }
    @Test
    public void testAddIssueWorklogTaggedItemFreshbooks() throws InterruptedException {
        List<Worklog> worklog = Lists.newArrayList();
        Date startDate = new Date();
        worklog.add(new WorklogImpl(worklogManager, issue, 123L, "fred", "work", startDate, "", null, 72000L));
        worklog.add( new WorklogImpl(worklogManager, issue, 124L, "fred", "more \n T-INV-12345", new Date(startDate.getTime()+100), "", null, 36000L));

        when(worklogService.getByIssue(serviceContext, issue)).thenReturn(worklog);
        when(issue.getUpdated()).thenReturn( new Timestamp( startDate.getTime() + 100 ));
        invoiceBuilder.addIssueWorklog(issue, 0);

        GenericInvoice invoice = invoiceBuilder.build();
        assertEquals(1, invoice.getItems().size() );
        assertEquals(((72000L)/3600)+".0",  invoice.getItems().get(0).getQuantity()+"" );
        assertTrue(invoice.getInvoiceNumber().contains("INV-KA-"));
    }

    @Test
    public void testAddIssueWorklogStartDate() throws InterruptedException {
        List<Worklog> worklog = Lists.newArrayList();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yy");

        Date startDate = new Date();
        worklog.add(new WorklogImpl(worklogManager, issue, 123L, "fred", "work \n T-INV-KA", startDate, "", null, 72000L));
        Date updateDate = new Date(startDate.getTime() + 1000 * 24 * 60 * 60);
        worklog.add( new WorklogImpl(worklogManager, issue, 124L, "fred", "more", updateDate, "", null, 36000L)); //a day later

        String update = dateFormat.format( updateDate );
        projectConfig.setStartDate( update );

        when(worklogService.getByIssue(serviceContext, issue)).thenReturn(worklog);
        when(issue.getUpdated()).thenReturn( new Timestamp( startDate.getTime() + 100 ));
        invoiceBuilder.addIssueWorklog(issue, 0);

        GenericInvoice invoice = invoiceBuilder.build();
        assertEquals(1, invoice.getItems().size() );
        GenericItem item = invoice.getItems().get(0);
        assertEquals(((36000L)/3600)+".0",  item.getQuantity()+"" );
        assertTrue(item.getDescription().contains("Summary"));
        assertTrue(item.getName().contains("KA-1"));
        assertTrue(invoice.getInvoiceNumber().contains("INV-KA-"));
    }

    class WorklogResultAnswer implements Answer<WorklogResult> {
        @Override
        public WorklogResult answer(InvocationOnMock invocation) throws Throwable {
            Object[] args = invocation.getArguments();
            WorklogInputParameters params = (WorklogInputParameters) args[1];
            assertTrue(params.getComment().contains(InvoiceBuilder.INVOICE_TAG_PREFIX + InvoiceBuilder.INVOICE_PREFIX));
            return null;  //To change body of implemented methods use File | Settings | File Templates.
        }
    }

}
