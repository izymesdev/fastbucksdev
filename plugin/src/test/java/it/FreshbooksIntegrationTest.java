package it;

import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.Maps;
import com.izymes.jira.fastbucks.api.FastbucksClient;
import com.izymes.jira.fastbucks.api.GenericResponse;
import com.izymes.jira.fastbucks.clients.freshbooks.FreshbooksClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Map;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FreshbooksIntegrationTest
{
    static final String AUTH_TOKEN = "1cd72777b738bcd9ea0e387916386340";
    static final String FB_API_URL = "https://izymes.freshbooks.com/api/2.1/xml-in";
    @Mock
    I18nResolver i18nResolver;

    @Test
    public void integrationTestSystem()
    {
        FastbucksClient client = new FreshbooksClient(i18nResolver, null);
        Map<String, String> config = Maps.newHashMap();
        config.put(FreshbooksClient.URL_KEY, FB_API_URL);
        config.put(FreshbooksClient.AUTH_TOKEN_KEY, AUTH_TOKEN);
        client.setConfig( config );
        when(i18nResolver.getText("fastbucks.project.config.client.test.organisation", "IZYMES")).thenReturn("Connected to IZYMES");
        GenericResponse response = client.testConnection(FastbucksClient.TEST_ENDPOINT.connection, null);
        System.out.println( response.getMessage() );
        assertTrue( response.getMessage().contains("IZYMES"));

    }
    @Test
    public void integrationTestClient()
    {
        FastbucksClient client = new FreshbooksClient(i18nResolver, null);
        Map<String, String> config = Maps.newHashMap();
        config.put(FreshbooksClient.URL_KEY, FB_API_URL);
        config.put(FreshbooksClient.AUTH_TOKEN_KEY, AUTH_TOKEN);
        client.setConfig( config );
        when(i18nResolver.getText(eq("fastbucks.project.config.client.test.contact"),eq(1), eq("Glennon"), anyString())).thenReturn("1 Contact found for Glennon");
        GenericResponse response = client.testConnection(FastbucksClient.TEST_ENDPOINT.contact, "Glennon");
        System.out.println( response.getMessage() );
        assertTrue( response.getMessage().contains("Glennon"));

    }
}
