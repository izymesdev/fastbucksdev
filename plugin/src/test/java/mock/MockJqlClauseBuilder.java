package mock;

import com.atlassian.jira.jql.builder.ConditionBuilder;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.query.Query;
import com.atlassian.query.clause.Clause;
import com.atlassian.query.operand.Operand;
import com.atlassian.query.operator.Operator;

import java.util.Collection;
import java.util.Date;

//project().eq(projectKey).and().updatedAfter(
public class MockJqlClauseBuilder implements JqlClauseBuilder {
    @Override
    public JqlQueryBuilder endWhere() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Query buildQuery() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder clear() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder defaultAnd() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder defaultOr() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder defaultNone() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder and() {
        return this;
    }

    @Override
    public JqlClauseBuilder or() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder not() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder sub() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder endsub() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder affectedVersion(String version) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder affectedVersion(String... versions) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder affectedVersionIsEmpty() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder affectedVersion() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder fixVersion(String version) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder fixVersion(String... versions) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder fixVersion(Long version) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder fixVersion(Long... versions) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder fixVersionIsEmpty() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder fixVersion() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder priority(String... priorities) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder priority() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder resolution(String... resolutions) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder unresolved() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder resolution() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder status(String... statuses) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder status() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder issueType(String... types) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder issueTypeIsStandard() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder issueTypeIsSubtask() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder issueType() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder description(String value) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder descriptionIsEmpty() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder description() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder summary(String value) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder summary() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder environment(String value) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder environmentIsEmpty() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder environment() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder comment(String value) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder comment() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder project(String... projects) {
        return this;
    }

    @Override
    public JqlClauseBuilder project(Long... pids) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder project() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder category(String... categories) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder category() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder createdAfter(Date startDate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder createdAfter(String startDate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder createdBetween(Date startDate, Date endDate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder createdBetween(String startDateString, String endDateString) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder created() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder updatedAfter(Date startDate) {
        return this;
    }

    @Override
    public JqlClauseBuilder updatedAfter(String startDate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder updatedBetween(Date startDate, Date endDate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder updatedBetween(String startDateString, String endDateString) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder updated() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder dueAfter(Date startDate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder dueAfter(String startDate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder dueBetween(Date startDate, Date endDate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder dueBetween(String startDateString, String endDateString) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder due() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder resolutionDateAfter(Date startDate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder resolutionDateAfter(String startDate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder resolutionDateBetween(Date startDate, Date endDate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder resolutionDateBetween(String startDateString, String endDateString) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder resolutionDate() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder reporterUser(String userName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder reporterInGroup(String groupName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder reporterIsCurrentUser() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder reporterIsEmpty() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder reporter() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder assigneeUser(String userName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder assigneeInGroup(String groupName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder assigneeIsCurrentUser() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder assigneeIsEmpty() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder assignee() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder component(String... components) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder component(Long... components) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder componentIsEmpty() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder component() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder labels(String... labels) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder labelsIsEmpty() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder labels() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder issue(String... keys) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder issueInHistory() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder issueInWatchedIssues() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder issueInVotedIssues() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder issue() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder issueParent(String... keys) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder issueParent() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder currentEstimate() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder originalEstimate() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder timeSpent() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder workRatio() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder level(String... levels) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder level() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder savedFilter(String... filters) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder savedFilter() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder votes() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder voterUser(String userName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder voterInGroup(String groupName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder voterIsCurrentUser() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder voterIsEmpty() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder voter() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder watches() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder watcherUser(String userName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder watcherInGroup(String groupName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder watcherIsCurrentUser() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder watcherIsEmpty() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder watcher() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder field(String jqlName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder customField(Long id) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addClause(Clause clause) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addDateCondition(String clauseName, Operator operator, Date date) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addDateCondition(String clauseName, Date... dates) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addDateCondition(String clauseName, Operator operator, Date... dates) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addDateCondition(String clauseName, Collection<Date> dates) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addDateCondition(String clauseName, Operator operator, Collection<Date> dates) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addDateRangeCondition(String clauseName, Date startDate, Date endDate) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addFunctionCondition(String clauseName, String functionName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addFunctionCondition(String clauseName, String functionName, String... args) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addFunctionCondition(String clauseName, String functionName, Collection<String> args) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addFunctionCondition(String clauseName, Operator operator, String functionName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addFunctionCondition(String clauseName, Operator operator, String functionName, String... args) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addFunctionCondition(String clauseName, Operator operator, String functionName, Collection<String> args) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addStringCondition(String clauseName, String clauseValue) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addStringCondition(String clauseName, String... clauseValues) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addStringCondition(String clauseName, Collection<String> clauseValues) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addStringCondition(String clauseName, Operator operator, String clauseValue) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addStringCondition(String clauseName, Operator operator, String... clauseValues) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addStringCondition(String clauseName, Operator operator, Collection<String> clauseValues) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addStringRangeCondition(String clauseName, String start, String end) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addNumberCondition(String clauseName, Long clauseValue) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addNumberCondition(String clauseName, Long... clauseValues) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addNumberCondition(String clauseName, Collection<Long> clauseValues) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addNumberCondition(String clauseName, Operator operator, Long clauseValue) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addNumberCondition(String clauseName, Operator operator, Long... clauseValues) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addNumberCondition(String clauseName, Operator operator, Collection<Long> clauseValues) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addNumberRangeCondition(String clauseName, Long start, Long end) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ConditionBuilder addCondition(String clauseName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addCondition(String clauseName, Operand operand) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addCondition(String clauseName, Operand... operands) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addCondition(String clauseName, Collection<? extends Operand> operands) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addCondition(String clauseName, Operator operator, Operand operand) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addCondition(String clauseName, Operator operator, Operand... operands) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addCondition(String clauseName, Operator operator, Collection<? extends Operand> operands) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addRangeCondition(String clauseName, Operand start, Operand end) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public JqlClauseBuilder addEmptyCondition(String clauseName) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Clause buildClause() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
