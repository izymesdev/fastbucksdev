package mock;

import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.google.common.collect.Maps;

import java.util.Date;
import java.util.Map;

public class MockPluginScheduler implements PluginScheduler {

    Map<String, ScheduledJob> scheduledJobs = Maps.newHashMap();
    Map<String, JobRunner> runningJobs = Maps.newHashMap();

    @Override
    public void scheduleJob(String jobKey, Class<? extends PluginJob> jobClass, Map<String, Object> jobDataMap, Date startTime, long repeatInterval) {
        scheduledJobs.put( jobKey, new ScheduledJob( jobClass, jobDataMap, startTime, repeatInterval));

        if ( runningJobs.get( jobKey ) != null ){
             System.out.println("Error cant schedule job");
            return;
        }
        JobRunner runningJob = new JobRunner( jobKey );
        runningJobs.put(jobKey, runningJob );
        Thread thread = new Thread( runningJob );

        thread.start();
    }

    @Override
    public void unscheduleJob(String jobKey) throws IllegalArgumentException{
        scheduledJobs.remove( jobKey );
        if ( runningJobs.get( jobKey ) == null ){
            System.out.println("Error cant stop job " + jobKey);
            throw new IllegalArgumentException( jobKey );
        }
        runningJobs.get(jobKey).setRunning( false );
        runningJobs.remove( jobKey );
    }

    public Map<String, Object> getScheduledJobDataMap( String jobKey ){
        return scheduledJobs.get( jobKey ).getJobDataMap();
    }
    public Date getScheduledJobStartTime( String key ){
        return scheduledJobs.get( key ).getStartTime();
    }
    public long getScheduledJobRepeatInterval( String key ){
        return scheduledJobs.get( key ).getRepeatInterval();
    }

    public class ScheduledJob{
        Class<? extends PluginJob> jobClass;
        Map<String, Object> jobDataMap;
        Date startTime;
        long repeatInterval;

        ScheduledJob(Class<? extends PluginJob> jobClass, Map<String, Object> jobDataMap, Date startTime, long repeatInterval) {
            this.jobClass = jobClass;
            this.jobDataMap = jobDataMap;
            this.startTime = startTime;
            this.repeatInterval = repeatInterval;
        }

        public Class<? extends PluginJob> getJobClass() {
            return jobClass;
        }

        public Map<String, Object> getJobDataMap() {
            return jobDataMap;
        }

        public Date getStartTime() {
            return startTime;
        }

        public long getRepeatInterval() {
            return repeatInterval;
        }
    }

    class JobRunner implements Runnable{
        private String jobKey;
        boolean running = true;
        JobRunner(String jobKey) {
            this.jobKey = jobKey;
        }

        @Override
        public void run() {
            Date startTime = getScheduledJobStartTime( jobKey );
            long delay = startTime.getTime()  - new Date().getTime();
            try {
                Thread.sleep( delay );
                while ( running ){
                    execute( jobKey );
                    if( running )
                        Thread.sleep( getScheduledJobRepeatInterval( jobKey ));
                }

            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (InstantiationException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (IllegalAccessException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        private void execute(String jobKey) throws IllegalAccessException, InstantiationException {
            PluginJob job = scheduledJobs.get(jobKey).getJobClass().newInstance();
            job.execute( getScheduledJobDataMap( jobKey ));
        }

        public synchronized void setRunning(boolean running) {
            this.running = running;
        }
    }
}
