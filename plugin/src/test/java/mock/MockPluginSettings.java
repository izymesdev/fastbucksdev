package mock;


import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.Properties;

public class MockPluginSettings implements PluginSettings {

    Map<String, Object> store = Maps.newHashMap();

    @Override
    public Object get(String key) {
        return store.get( key );
    }

    @Override
    public Object put(String key, Object value) {
        if ( value instanceof String || value instanceof List || value instanceof Properties || value instanceof Map) {
        store.put( key, value );
        return value;
        } else {
            throw new RuntimeException(value.getClass() + " not supported");
        }

    }

    @Override
    public Object remove(String key) {
        store.remove( key );
        return key;
    }
}
