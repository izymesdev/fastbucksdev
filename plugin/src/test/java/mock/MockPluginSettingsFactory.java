package mock;


import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.collect.Maps;

import java.util.Map;

public class MockPluginSettingsFactory implements PluginSettingsFactory {

    PluginSettings globalSettings = new MockPluginSettings();
    Map<String, PluginSettings> keySettings = Maps.newHashMap();


    @Override
    public PluginSettings createSettingsForKey(String key) {
        PluginSettings pluginSettings = keySettings.get( key );
        if ( pluginSettings == null ){
            pluginSettings = new MockPluginSettings();
            keySettings.put( key, pluginSettings );
        }
        return pluginSettings;
    }

    @Override
    public PluginSettings createGlobalSettings() {
        return globalSettings;
    }
}
